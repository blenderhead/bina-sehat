<?php
	
	class EmployeeDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				for($i=0;$i<count($data['id']);$i++)
				{
					Employee::where('id',$data['id'][$i])->delete();
					MedCheck::where('employee_id',$data['id'][$i])->delete();
					Hematology::where('employee_id',$data['id'][$i])->delete();
					UrineCheck::where('employee_id',$data['id'][$i])->delete();
				}
				
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}