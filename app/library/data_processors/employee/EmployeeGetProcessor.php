<?php
	
	class EmployeeGetProcessor extends BaseProcessor
	{
		public function process()
		{
			try
			{
	            $employees = Employee::all();
	            
	            $data = Datatable::collection($employees);
	            
	            $this->output = $data
	                ->addColumn('', function($model) {
	                    return '<input type="checkbox" class="employee" value="' . $model->id . '">';
	                })
	                ->addColumn('id', function($model) {
	                    return $model->id;
	                })
	                ->addColumn('nik', function($model) {
	                    return $model->nik;
	                })
	                ->addColumn('name', function($model) {
	                    return $model->first_name . ' ' . $model->middle_name . ' ' . $model->last_name;
	                })
	                ->addColumn('departemen', function($model) {
	                    return $model->dpt->name;
	                })
	                ->addColumn('action', function($model) {
	                    $action = '';

	                    $action .= '<a title="Edit karyawan" data-toggle="tooltip" href="' . URL::to('/') . '/employee/edit?id=' . $model->id . '" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="' . $model->id .'"><i class="glyphicon glyphicon-pencil"></i></a>';
						$action .= '<button title="Hapus karyawan" data-toggle="tooltip" type="button" class="btn btn-warning btn-circle margin-right-5 delete" data-id="' . $model->id . '"><i class="glyphicon glyphicon-remove"></i></button>';
						$action .= '<a title="Tambah laporan" data-toggle="tooltip" href="' . URL::to('/') . '/medcheck/add/?id=' . $model->id . '" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="' . $model->id . '"><i class="glyphicon glyphicon-stats"></i></a>';
						$action .= '<a title="Lihat laporan" data-toggle="tooltip" href="' . URL::to('/') . '/medcheck?id=' . $model->id .'" type="button" class="btn btn-success btn-circle edit" data-id="' . $model->id . '"><i class="glyphicon glyphicon-folder-open"></i></a>';

	                    return $action;
	                })
	                ->searchColumns('name', 'nik', 'departemen')
	                ->orderColumns('name')
	                ->make();
				
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}