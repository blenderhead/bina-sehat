<?php
	
	class EmployeeEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$employee = Employee::find($data['id']);
				$employee->nik = $data['nik'];
				$employee->first_name = $data['first_name'];
				$employee->middle_name = $data['middle_name'];
				$employee->last_name = $data['last_name'];
				$employee->age = $data['age'];
				$employee->gender = $data['gender'];
				$employee->department = $data['department'];
				$employee->save();

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}