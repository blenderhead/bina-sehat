<?php
	
	use PHPExcel_IOFactory;

	class EmployeeImportProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$valid_mimes = Config::get('valid_excel');

				if(!in_array($data['file_import']->getMimeType(), $valid_mimes))
				{
					throw new Exception("File tdk dapat di import", 1000);
				}

				$destination = Config::get('path.import_path');

				$upload_manager = new UploadProcessor($data['file_import'], $destination);
				$upload_manager->upload();

				$filename = $upload_manager->getFilename();

				$file = $destination . $filename;

				$inputFileType = PHPExcel_IOFactory::identify($file);
				$reader = PHPExcel_IOFactory::createReader($inputFileType);

	     		$excel = $reader->load($file);

				$writer = PHPExcel_IOFactory::createWriter($excel, 'CSV');

				$csv_filename = 'import-' . date('Y-m-d', time()) . '.csv';

				$writer->save($destination . $csv_filename);

				$csv = new Pingpong\CsvReader\CsvReader($destination . $csv_filename);

				$output = $csv->getData()->toArray();

				/*
				[0]=> No
			    [1]=> INTERNAL ID
			    [2]=> NIK
			    [3]=> Nama
			    [4]=> Tanggal Lahir
			    [5]=> Bagian
			    [6]=> Status
			    */
			    
				for($i=0;$i<count($output);$i++)
				{
					if($output[$i])
					{
						if(Employee::checkDoubleNik($output[$i][2]) != false)
						{
							$employee = new Employee();
			                $employee->nik = $output[$i][2];

			                $name = Helper::processName($output[$i][3]);
			                $employee->first_name = ucfirst(strtolower($name['first']));
			                $employee->middle_name = ucfirst(strtolower($name['middle']));
			                $employee->last_name = ucfirst(strtolower($name['last']));

			                $employee->age = 20;
			                $employee->gender = 'l';
			                $employee->department = Department::getDepartmentId($output[$i][5]);
			                $employee->save();
						}
					}
				}

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}