<?php
	
	class AllXlsProcessor extends BaseProcessor
	{
		public function process()
		{
			try
			{
				$reports = MedCheck::all();

	            Excel::create('AllReport', function($excel) use ($reports) {

	                $excel->sheet('Sheet 1', function($sheet) use($reports) {

	                    $row_count = array();

	                    for($i=0;$i<=count($reports) + 1;$i++)
	                    {
	                        array_push($row_count, 25);
	                    }

	                    $count = count($reports) + 1;

	                    $sheet->setBorder("A1:Z" . $count, 'thin');

	                    $iterator = 1;

	                    $result = array();

	                    foreach($reports as $report)
	                    {
	                        $output = array(
	                            'No' => $iterator,
	                            'NIK' => $report->employee->nik,
	                            'Nama Peserta' => $report->employee->first_name . ' ' . $report->employee->middle_name . ' ' . $report->employee->last_name,
	                            'Usia' => $report->employee->age,
	                            'L/P' => strtoupper($report->employee->gender),
	                            'Bagian' => $report->employee->dpt->name,
	                            'Tanggal Periksa' => date('j F Y', strtotime($report->check_date)),
	                            'Merokok' => $report->is_smoking ? 'Y' : 'T',
	                            'Alkohol' => $report->is_consuming_alcohol ? 'Y' : 'T',
	                            'Olah Raga' => $report->is_exercising ? 'Y' : 'T',
	                            'BB' => $report->weight,
	                            'TB' => $report->height,
	                            'Tensi' => $report->blood_pressure,
	                            'HB' => $report->hematology->hemoglobin_val,
	                            'Leu' => $report->hematology->leukosit_val,
	                            'PCV' => $report->hematology->pcv_val,
	                            'Trombosit' => $report->hematology->trombosit_val,
	                            'LED' => $report->hematology->led_val,
	                            'HbsAg' => Helper::processPolar($report->imuno->hbsag_status),
	                            'Wrn' => Helper::processUrine($report->urine->color_val),
	                            'pH' => $report->urine->ph_val,
	                            'BJ' => $report->urine->density_val,
	                            'Lain-lain' => Helper::processPolar($report->urine->protein_val),
	                            'Epl' => $report->urine->epitel_val,
	                            'Leu' => $report->urine->leukosit_val,
	                            'Eri' => $report->urine->eritrosit_val,
	                            'Bak' => Helper::processPolar($report->urine->bacteria_val)
	                        );
	                        
	                        array_push($result, $output);

	                        $iterator++;
	                    }

	                    $sheet->setHeight($row_count);

	                    $sheet->setBorder('A1:Z1', 'thin');

	                    $sheet->cells('A1:Z1', function($cell) {

	                        $cell->setFont(array(
	                            'bold'       => true
	                        ));

	                        $cell->setAlignment('center');

	                    });

	                    $sheet->fromArray($result);

	                });

            	})->download('xlsx');
				
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}