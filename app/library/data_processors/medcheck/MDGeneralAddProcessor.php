<?php
	
	class MDGeneralAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$general_data = new MedCheck();
				$general_data->employee_id = $data['employee_id'];
				$general_data->weight = $data['weight'];
				$general_data->height = $data['height'];
				$general_data->blood_pressure = $data['blood_pressure'];
				$general_data->is_smoking = $data['is_smoking'];
				$general_data->is_consuming_alcohol = $data['is_consuming_alcohol'];
				$general_data->is_exercising = $data['is_exercising'];
				$general_data->check_date = date('Y-m-d', time());

				Session::put('general_data',$general_data);
				
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}