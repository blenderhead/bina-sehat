<?php
	
	class MedDeleteByDateProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				for($i=0;$i<count($data['id']);$i++)
				{
					MedCheck::where('check_date',$data['id'][$i])->delete();
					Hematology::where('check_date',$data['id'][$i])->delete();
					ImunoSerology::where('check_date',$data['id'][$i])->delete();
					UrineCheck::where('check_date',$data['id'][$i])->delete();
					Result::where('check_date',$data['id'][$i])->delete();
				}
				
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}