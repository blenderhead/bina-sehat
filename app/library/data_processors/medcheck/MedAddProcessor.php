<?php
	
	class MedAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$check_date = date('Y-m-d', time());

				$general_data = new MedCheck();
				$general_data->employee_id = $data['employee_id'];
				$general_data->complain = $data['complain'];
				$general_data->medical_history = $data['medical_history'];
				$general_data->weight = $data['weight'];
				$general_data->height = $data['height'];
				$general_data->blood_pressure = $data['blood_pressure'];
				$general_data->is_smoking = $data['is_smoking'];
				$general_data->is_consuming_alcohol = $data['is_consuming_alcohol'];
				$general_data->is_exercising = $data['is_exercising'];
				$general_data->jvp = $data['jvp'];
				$general_data->apex_beat = $data['apex_beat'];
				$general_data->cardiac_murmur = $data['cardiac_murmur'];
				$general_data->murmur = $data['murmur'];
				$general_data->ronkhi = $data['ronkhi'];
				$general_data->wheezing = $data['wheezing'];
				$general_data->check_date = $check_date;
				$general_data->save();

				$hema_data = new Hematology();
				$hema_data->employee_id = $data['employee_id'];
				$hema_data->med_check_id = $general_data->id;
				$hema_data->hemoglobin_val = $data['hemoglobin_val'];
				$hema_data->hemoglobin_ref = $data['hemoglobin_ref'];
				$hema_data->leukosit_val = $data['leukosit_val'];
				$hema_data->leukosit_ref = $data['leukosit_ref'];
				$hema_data->pcv_val = $data['pcv_val'];
				$hema_data->pcv_ref = $data['pcv_ref'];
				$hema_data->trombosit_val = $data['trombosit_val'];
				$hema_data->trombosit_ref = $data['trombosit_ref'];
				$hema_data->led_val = $data['led_val'];
				$hema_data->led_ref = $data['led_ref'];
				$hema_data->check_date = $check_date;
				$hema_data->save();

				$imuno_data = new ImunoSerology();
				$imuno_data->employee_id = $data['employee_id'];
				$imuno_data->med_check_id = $general_data->id;
				$imuno_data->hbsag_status = $data['hbsag_status'];
				$imuno_data->hbsag_reference = $data['hbsag_reference'];
				$imuno_data->ict_tbc = $data['ict_tbc'];
				$imuno_data->ict_reference = $data['ict_reference'];
				$imuno_data->check_date = $check_date;
				$imuno_data->save();

				$urine_data = new UrineCheck();
				$urine_data->employee_id = $data['employee_id'];
				$urine_data->med_check_id = $general_data->id;
				$urine_data->color_val = $data['color_val'];
				$urine_data->color_val_ref = $data['color_val_ref'];
				$urine_data->density_val = $data['density_val'];
				$urine_data->density_val_ref = $data['density_val_ref'];
				$urine_data->ph_val = $data['ph_val'];
				$urine_data->ph_val_ref = $data['ph_val_ref'];
				$urine_data->leukosit_estrase_val = $data['leukosit_estrase_val'];
				$urine_data->leukosit_estrase_val_ref = $data['leukosit_estrase_val_ref'];
				$urine_data->nitrit_val = $data['nitrit_val'];
				$urine_data->nitrit_val_ref = $data['nitrit_val_ref'];
				$urine_data->protein_val = $data['protein_val'];
				$urine_data->protein_val_ref = $data['protein_val_ref'];
				$urine_data->glucose_val = $data['glucose_val'];
				$urine_data->glucose_val_ref = $data['glucose_val_ref'];
				$urine_data->keton_val = $data['keton_val'];
				$urine_data->keton_val_ref = $data['keton_val_ref'];
				$urine_data->urobilinogen_val = $data['urobilinogen_val'];
				$urine_data->urobilinogen_val_ref = $data['urobilinogen_val_ref'];
				$urine_data->bilirubin_val = $data['bilirubin_val'];
				$urine_data->bilirubin_val_ref = $data['bilirubin_val_ref'];
				$urine_data->eritrosit_val = $data['eritrosit_val'];
				$urine_data->eritrosit_val_ref = $data['eritrosit_val_ref'];
				$urine_data->leukosit_val = $data['u_leukosit_val'];
				$urine_data->leukosit_val_ref = $data['u_leukosit_val_ref'];
				$urine_data->epitel_val = $data['epitel_val'];
				$urine_data->epitel_val_ref = $data['epitel_val_ref'];
				$urine_data->bacteria_val = $data['bacteria_val'];
				$urine_data->bacteria_val_ref = $data['bacteria_val_ref'];
				$urine_data->silinder_val = $data['silinder_val'];
				$urine_data->silinder_val_ref = $data['silinder_val_ref'];
				$urine_data->kristal_val = $data['kristal_val'];
				$urine_data->kristal_val_ref = $data['kristal_val_ref'];
				$urine_data->check_date = $check_date;
				$urine_data->save();

				$result_data = new Result();
				$result_data->employee_id = $data['employee_id'];
				$result_data->med_check_id = $general_data->id;
				$result_data->result_fisik = $data['result_fisik'];
				$result_data->result_hema = $data['result_hema'];
				$result_data->result_urine = $data['result_urine'];
				$result_data->result_hbsag = $data['result_hbsag'];
				$result_data->result_tbc = $data['result_tbc'];
				$result_data->result_final = $data['result_final'];
				$result_data->check_date = $check_date;
				$result_data->save();
				
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}