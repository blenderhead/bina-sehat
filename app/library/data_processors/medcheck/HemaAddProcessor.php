<?php
	
	class HemaAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$hema_data = new Hematology();
				$hema_data->employee_id = $data['employee_id'];
				$hema_data->hemoglobin_val = $data['hemoglobin_val'];
				$hema_data->leukosit_val = $data['leukosit_val'];
				$hema_data->pcv_val = $data['pcv_val'];
				$hema_data->trombosit_val = $data['trombosit_val'];
				$hema_data->led_val = $data['led_val'];
				$hema_data->hbsag_status = $data['hbsag_status'];
				$hema_data->check_date = date('Y-m-d', time());
				
				Session::put('hema_data',$hema_data);
				
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}