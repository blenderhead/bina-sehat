<?php
	
	class MedDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				for($i=0;$i<count($data['id']);$i++)
				{
					MedCheck::where('id',$data['id'][$i])->delete();
					Hematology::where('med_check_id',$data['id'][$i])->delete();
					ImunoSerology::where('med_check_id',$data['id'][$i])->delete();
					UrineCheck::where('med_check_id',$data['id'][$i])->delete();
					Result::where('med_check_id',$data['id'][$i])->delete();
				}
				
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}