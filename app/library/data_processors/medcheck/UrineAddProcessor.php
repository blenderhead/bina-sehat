<?php
	
	class UrineAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$check_date = date('Y-m-d', time());

				$general_data = Session::get('general_data');
				$general_data->save();

				$hema_data = Session::get('hema_data');
				$hema_data->med_check_id = $general_data->id;
				$hema_data->save();

				$urine_data = new UrineCheck();
				$urine_data->employee_id = $data['employee_id'];
				$urine_data->med_check_id = $general_data->id;
				$urine_data->color_val = $data['color_val'];
				$urine_data->ph_val = $data['ph_val'];
				$urine_data->density_val = $data['density_val'];
				$urine_data->other_val = $data['other_val'];
				$urine_data->epitel_val = $data['epitel_val'];
				$urine_data->leukosit_val = $data['leukosit_val'];
				$urine_data->eritrosit_val = $data['eritrosit_val'];
				$urine_data->bacteria_status = $data['bacteria_status'];
				$urine_data->check_date = $check_date;
				$urine_data->save();

				Session::forget('general_data');
				Session::forget('hema_data');
				
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}