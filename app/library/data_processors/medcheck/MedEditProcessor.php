<?php
	
	class MedEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$general = MedCheck::find($data['report_id']);
				$general->employee_id = $data['employee_id'];
				$general->complain = $data['complain'];
				$general->medical_history = $data['medical_history'];
				$general->weight = $data['weight'];
				$general->height = $data['height'];
				$general->blood_pressure = $data['blood_pressure'];
				$general->is_smoking = $data['is_smoking'];
				$general->is_consuming_alcohol = $data['is_consuming_alcohol'];
				$general->is_exercising = $data['is_exercising'];
				$general->jvp = $data['jvp'];
				$general->apex_beat = $data['apex_beat'];
				$general->cardiac_murmur = $data['cardiac_murmur'];
				$general->murmur = $data['murmur'];
				$general->ronkhi = $data['ronkhi'];
				$general->wheezing = $data['wheezing'];
				$general->save();
				
				$hematology = Hematology::find($general->hematology->id);
				$hematology->hemoglobin_val = $data['hemoglobin_val'];
				$hematology->hemoglobin_ref = $data['hemoglobin_ref'];
				$hematology->leukosit_val = $data['leukosit_val'];
				$hematology->leukosit_ref = $data['leukosit_ref'];
				$hematology->pcv_val = $data['pcv_val'];
				$hematology->pcv_ref = $data['pcv_ref'];
				$hematology->trombosit_val = $data['trombosit_val'];
				$hematology->trombosit_ref = $data['trombosit_ref'];
				$hematology->led_val = $data['led_val'];
				$hematology->led_ref = $data['led_ref'];
				$hematology->save();
				$general->hematology()->update($hematology->toArray());

				$imuno = ImunoSerology::find($general->imuno->id);
				$imuno->hbsag_status = $data['hbsag_status'];
				$imuno->hbsag_reference = $data['hbsag_reference'];
				$imuno->ict_tbc = $data['ict_tbc'];
				$imuno->ict_reference = $data['ict_reference'];
				$general->imuno()->update($imuno->toArray());

				$urine = UrineCheck::find($general->urine->id);
				$urine->color_val = $data['color_val'];
				$urine->color_val_ref = $data['color_val_ref'];
				$urine->density_val = $data['density_val'];
				$urine->density_val_ref = $data['density_val_ref'];
				$urine->ph_val = $data['ph_val'];
				$urine->ph_val_ref = $data['ph_val_ref'];
				$urine->leukosit_estrase_val = $data['leukosit_estrase_val'];
				$urine->leukosit_estrase_val_ref = $data['leukosit_estrase_val_ref'];
				$urine->nitrit_val = $data['nitrit_val'];
				$urine->nitrit_val_ref = $data['nitrit_val_ref'];
				$urine->protein_val = $data['protein_val'];
				$urine->protein_val_ref = $data['protein_val_ref'];
				$urine->glucose_val = $data['glucose_val'];
				$urine->glucose_val_ref = $data['glucose_val_ref'];
				$urine->keton_val = $data['keton_val'];
				$urine->keton_val_ref = $data['keton_val_ref'];
				$urine->urobilinogen_val = $data['urobilinogen_val'];
				$urine->urobilinogen_val_ref = $data['urobilinogen_val_ref'];
				$urine->bilirubin_val = $data['bilirubin_val'];
				$urine->bilirubin_val_ref = $data['bilirubin_val_ref'];
				$urine->eritrosit_val = $data['eritrosit_val'];
				$urine->eritrosit_val_ref = $data['eritrosit_val_ref'];
				$urine->leukosit_val = $data['u_leukosit_val'];
				$urine->leukosit_val_ref = $data['u_leukosit_val_ref'];
				$urine->epitel_val = $data['epitel_val'];
				$urine->epitel_val_ref = $data['epitel_val_ref'];
				$urine->bacteria_val = $data['bacteria_val'];
				$urine->bacteria_val_ref = $data['bacteria_val_ref'];
				$urine->silinder_val = $data['silinder_val'];
				$urine->silinder_val_ref = $data['silinder_val_ref'];
				$urine->kristal_val = $data['kristal_val'];
				$urine->kristal_val_ref = $data['kristal_val_ref'];
				$general->urine()->update($urine->toArray());
				
				$result = Result::find($general->result->id);
				$result->result_fisik = $data['result_fisik'];
				$result->result_hema = $data['result_hema'];
				$result->result_urine = $data['result_urine'];
				$result->result_hbsag = $data['result_hbsag'];
				$result->result_tbc = $data['result_tbc'];
				$result->result_final = $data['result_final'];
				$general->result()->update($result->toArray());

				$general->save();

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}