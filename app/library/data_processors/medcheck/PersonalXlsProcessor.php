<?php
	
	class PersonalXlsProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$employee = Employee::find($data['employee_id']);
	            $filename = strtolower($employee->first_name . '_' . $employee->middle_name . '_' . $employee->last_name);

	            Excel::create($filename, function($excel) use ($employee) {

	                $excel->sheet('Sheet 1', function($sheet) use($employee) {

	                    $row_count = array();

	                    for($i=0;$i<=count($employee->medCheck) + 1;$i++)
	                    {
	                        array_push($row_count, 25);
	                    }

	                    $count = count($employee->medCheck) + 1;

	                    $sheet->setBorder("A1:U" . $count, 'thin');

	                    $iterator = 1;

	                    $result = array();

	                    foreach($employee->medCheck as $report)
	                    {
	                        $output = array(
	                            'No' => $iterator,
	                            'Tanggal Periksa' => date('j F Y', strtotime($report->check_date)),
	                            'Merokok' => $report->is_smoking ? 'Y' : 'T',
	                            'Alkohol' => $report->is_consuming_alcohol ? 'Y' : 'T',
	                            'Olah Raga' => $report->is_exercising ? 'Y' : 'T',
	                            'BB' => $report->weight,
	                            'TB' => $report->height,
	                            'Tensi' => $report->blood_pressure,
	                            'HB' => $report->hematology->hemoglobin_val,
	                            'Leu' => $report->hematology->leukosit_val,
	                            'PCV' => $report->hematology->pcv_val,
	                            'Trombosit' => $report->hematology->trombosit_val,
	                            'LED' => $report->hematology->led_val,
	                            'HbsAg' => Helper::processPolar($report->imuno->hbsag_status),
	                            'Wrn' => Helper::processUrine($report->urine->color_val),
	                            'pH' => $report->urine->ph_val,
	                            'BJ' => $report->urine->density_val,
	                            'Lain-lain' => Helper::processPolar($report->urine->protein_val),
	                            'Epl' => $report->urine->epitel_val,
	                            'Leu' => $report->urine->leukosit_val,
	                            'Eri' => $report->urine->eritrosit_val,
	                            'Bak' => Helper::processPolar($report->urine->bacteria_val)
	                        );
	                        
	                        array_push($result, $output);

	                        $iterator++;
	                    }

	                    $sheet->setHeight($row_count);

	                    $sheet->setBorder('A1:U1', 'thin');

	                    $sheet->cells('A1:U1', function($cell) {

	                        $cell->setFont(array(
	                            'bold'       => true
	                        ));

	                        $cell->setAlignment('center');

	                    });

	                    $sheet->fromArray($result);

	                });

            	})->download('xlsx');
				
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}