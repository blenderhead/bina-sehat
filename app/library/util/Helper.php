<?php

	class Helper
	{
		public static function processGender($gender_id)
        {
            switch ($gender_id) 
            {
                case 'p':
                    return 'Perempuan';
                    break;
                
                case 'l':
                    return 'Laki-laki';
                    break;
            }
        }

        public static function processPolar($polar_id)
        {
            switch ($polar_id) 
            {
                case 0:
                    return 'Negatif';
                    break;
                
                case 1:
                    return 'Positif';
                    break;
            }
        }

        public static function processUrine($urine_val)
        {
            /*
            'kk' => 'Kuning Keruh',
            'km' => 'Kuning Muda',
            'kj' => 'Kuning Jernih',
            'kak' => 'Kuning Agak Keruh'
            */
            switch ($urine_val) 
            {
                case 'kk':
                    return 'Kuning Keruh';
                    break;
                
                case 'km':
                    return 'Kuning Muda';
                    break;

                case 'kj':
                    return 'Kuning Jernih';
                    break;

                case 'kak':
                    return 'Kuning Agak Keruh';
                    break;
            }
        }

        public static function processName($name)
        {
            $parts = explode(' ', $name);
            $first = array_shift($parts);
            $last = array_pop($parts);
            $middle = trim(implode(' ', $parts));

            return compact("first", "last", "middle");
        }
	}