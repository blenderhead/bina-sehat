<?php

	class UploadCreator
	{
		public function createDirScturct()
        {
            $upload_root = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR;

            $pdf_upload_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR;

            $import_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'excel_import' . DIRECTORY_SEPARATOR;

            if (!File::exists($upload_root))
            {
                File::makeDirectory($upload_root, $mode = 0777, true, true);
            }

            if (!File::exists($pdf_upload_path))
            {
                File::makeDirectory($pdf_upload_path, $mode = 0777, true, true);
            }

            if (!File::exists($import_path))
            {
                File::makeDirectory($import_path, $mode = 0777, true, true);
            }
        }
	}