<?php
	
	class HemaAddRepository extends BaseRepository
	{
		private $employee_id;
		private $hemoglobin_val;
		private $leukosit_val;
		private $pcv_val;
		private $trombosit_val;
		private $led_val;
		private $hbsag_status;

		public function getInput()
		{
			$this->employee_id = Input::get('employee_id');
			$this->hemoglobin_val = Input::get('hemoglobin_val');
			$this->leukosit_val = Input::get('leukosit_val');
			$this->pcv_val = Input::get('pcv_val');
			$this->trombosit_val = Input::get('trombosit_val');
			$this->led_val = Input::get('led_val');
			$this->hbsag_status = Input::get('hbsag_status');
		}

		public function setValidationData()
		{
			$this->data = array(
				'employee_id' => $this->employee_id,
	            'hemoglobin_val' => $this->hemoglobin_val,
	            'leukosit_val' => $this->leukosit_val,
	            'pcv_val' => $this->pcv_val,
	            'trombosit_val' => $this->trombosit_val,
	            'led_val' => $this->led_val,
	            'hbsag_status' => $this->hbsag_status,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'employee_id' => 'required',
	            'hemoglobin_val' => 'required|numeric',
	            'leukosit_val' => 'required|numeric',
	            'pcv_val' => 'required|numeric',
	            'trombosit_val' => 'required|numeric',
	            'led_val' => 'required|numeric',
	            'hbsag_status' => 'required'
			);
		}
	}