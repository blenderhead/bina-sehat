<?php
	
	class UrineAddRepository extends BaseRepository
	{
		private $employee_id;
		private $color_val;
		private $ph_val;
		private $density_val;
		private $other_val;
		private $epitel_val;
		private $leukosit_val;
		private $eritrosit_val;
		private $bacteria_status;

		public function getInput()
		{
			$this->employee_id = Input::get('employee_id');
			$this->color_val = Input::get('color_val');
			$this->ph_val = Input::get('ph_val');
			$this->density_val = Input::get('density_val');
			$this->other_val = Input::get('other_val');
			$this->epitel_val = Input::get('epitel_val');
			$this->leukosit_val = Input::get('leukosit_val');
			$this->eritrosit_val = Input::get('eritrosit_val');
			$this->bacteria_status = Input::get('bacteria_status');
		}

		public function setValidationData()
		{
			$this->data = array(
				'employee_id' => $this->employee_id,
	            'color_val' => $this->color_val,
	            'ph_val' => $this->ph_val,
	            'density_val' => $this->density_val,
	            'other_val' => $this->other_val,
	            'epitel_val' => $this->epitel_val,
	            'leukosit_val' => $this->leukosit_val,
	            'eritrosit_val' => $this->eritrosit_val,
	            'bacteria_status' => $this->bacteria_status,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'employee_id' => 'required',
	            'ph_val' => 'required|numeric',
	            'density_val' => 'required|numeric',
	            'other_val' => 'required',
	            'epitel_val' => 'required',
	            'leukosit_val' => 'required',
	            'eritrosit_val' => 'required',
	            'bacteria_status' => 'required'
			);
		}
	}