<?php
	
	class MDGeneralAddRepository extends BaseRepository
	{
		private $employee_id;
		private $weight;
		private $height;
		private $blood_pressure;
		private $is_smoking;
		private $is_consuming_alcohol;
		private $is_exercising;

		public function getInput()
		{
			$this->employee_id = Input::get('employee_id');
			$this->weight = Input::get('weight');
			$this->height = Input::get('height');
			$this->blood_pressure = Input::get('blood_pressure');
			$this->is_smoking = Input::get('is_smoking');
			$this->is_consuming_alcohol = Input::get('is_consuming_alcohol');
			$this->is_exercising = Input::get('is_exercising');
		}

		public function setValidationData()
		{
			$this->data = array(
				'employee_id' => $this->employee_id,
	            'weight' => $this->weight,
	            'height' => $this->height,
	            'blood_pressure' => $this->blood_pressure,
	            'is_smoking' => $this->is_smoking == 'on' ? 1 : 0,
	            'is_consuming_alcohol' => $this->is_consuming_alcohol == 'on' ? 1 : 0,
	            'is_exercising' => $this->is_exercising == 'on' ? 1 : 0,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'employee_id' => 'required',
	            'weight' => 'required|numeric',
	            'height' => 'required|numeric',
	            'blood_pressure' => 'required|numeric'
			);
		}
	}