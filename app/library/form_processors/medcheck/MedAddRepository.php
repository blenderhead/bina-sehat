<?php
	
	class MedAddRepository extends BaseRepository
	{
		private $employee_id;

		private $complain;
		private $medical_history;
		private $weight;
		private $height;
		private $blood_pressure;
		private $is_smoking;
		private $is_consuming_alcohol;
		private $is_exercising;
		private $jvp;
		private $apex_beat;
		private $cardiac_murmur;
		private $murmur;
		private $ronkhi;
		private $wheezing;

		private $hemoglobin_val;
		private $hemoglobin_ref;
		private $leukosit_val;
		private $leukosit_ref;
		private $pcv_val;
		private $pcv_ref;
		private $trombosit_val;
		private $trombosit_ref;
		private $led_val;
		private $led_ref;

		private $hbsag_status;
		private $hbsag_reference;
		private $ict_tbc;
		private $ict_reference;

		private $color_val;
		private $color_val_ref;
		private $density_val;
		private $density_val_ref;
		private $ph_val;
		private $ph_val_ref;
		private $leukosit_estrase_val;
		private $leukosit_estrase_val_ref;
		private $nitrit_val;
		private $nitrit_val_ref;
		private $protein_val;
		private $protein_val_ref;
		private $glucose_val;
		private $glucose_val_ref;
		private $keton_val;
		private $keton_val_ref;
		private $urobilinogen_val;
		private $urobilinogen_val_ref;
		private $bilirubin_val;
		private $bilirubin_val_ref;
		private $eritrosit_val;
		private $eritrosit_val_ref;
		private $u_leukosit_val;
		private $u_leukosit_val_ref;
		private $epitel_val;
		private $epitel_val_ref;
		private $bacteria_val;
		private $bacteria_val_ref;
		private $silinder_val;
		private $silinder_val_ref;
		private $kristal_val;
		private $kristal_val_ref;

		private $result_fisik;
		private $result_hema;
		private $result_urine;
		private $result_hbsag;
		private $result_tbc;
		private $result_final;

		public function getInput()
		{
			$this->employee_id = Input::get('employee_id');

			$this->complain = Input::get('complain');
			$this->medical_history = Input::get('medical_history');
			$this->weight = Input::get('weight');
			$this->height = Input::get('height');
			$this->blood_pressure = Input::get('blood_pressure');
			$this->is_smoking = Input::get('is_smoking');
			$this->is_consuming_alcohol = Input::get('is_consuming_alcohol');
			$this->is_exercising = Input::get('is_exercising');
			$this->jvp = Input::get('jvp');
			$this->apex_beat = Input::get('apex_beat');
			$this->cardiac_murmur = Input::get('cardiac_murmur');
			$this->murmur = Input::get('murmur');
			$this->ronkhi = Input::get('ronkhi');
			$this->wheezing = Input::get('wheezing');

			$this->hemoglobin_val = Input::get('hemoglobin_val');
			$this->hemoglobin_ref = Input::get('hemoglobin_ref');
			$this->leukosit_val = Input::get('leukosit_val');
			$this->leukosit_ref = Input::get('leukosit_ref');
			$this->pcv_val = Input::get('pcv_val');
			$this->pcv_ref = Input::get('pcv_ref');
			$this->trombosit_val = Input::get('trombosit_val');
			$this->trombosit_ref = Input::get('trombosit_ref');
			$this->led_val = Input::get('led_val');
			$this->led_ref = Input::get('led_ref');

			$this->hbsag_status = Input::get('hbsag_status');
			$this->hbsag_reference = Input::get('hbsag_reference');
			$this->ict_tbc = Input::get('ict_tbc');
			$this->ict_reference = Input::get('ict_reference');

			$this->color_val = Input::get('color_val');
			$this->color_val_ref = Input::get('color_val_ref');
			$this->density_val = Input::get('density_val');
			$this->density_val_ref = Input::get('density_val_ref');
			$this->ph_val = Input::get('ph_val');
			$this->ph_val_ref = Input::get('ph_val_ref');
			$this->leukosit_estrase_val = Input::get('leukosit_estrase_val');
			$this->leukosit_estrase_val_ref = Input::get('leukosit_estrase_val_ref');
			$this->nitrit_val = Input::get('nitrit_val');
			$this->nitrit_val_ref = Input::get('nitrit_val_ref');
			$this->protein_val = Input::get('protein_val');
			$this->protein_val_ref = Input::get('protein_val_ref');
			$this->glucose_val = Input::get('glucose_val');
			$this->glucose_val_ref = Input::get('glucose_val_ref');
			$this->keton_val = Input::get('keton_val');
			$this->keton_val_ref = Input::get('keton_val_ref');
			$this->urobilinogen_val = Input::get('urobilinogen_val');
			$this->urobilinogen_val_ref = Input::get('urobilinogen_val_ref');
			$this->bilirubin_val = Input::get('bilirubin_val');
			$this->bilirubin_val_ref = Input::get('bilirubin_val_ref');
			$this->eritrosit_val = Input::get('eritrosit_val');
			$this->eritrosit_val_ref = Input::get('eritrosit_val_ref');
			$this->u_leukosit_val = Input::get('u_leukosit_val');
			$this->u_leukosit_val_ref = Input::get('u_leukosit_val_ref');
			$this->epitel_val = Input::get('epitel_val');
			$this->epitel_val_ref = Input::get('epitel_val_ref');
			$this->bacteria_val = Input::get('bacteria_val');
			$this->bacteria_val_ref = Input::get('bacteria_val_ref');
			$this->silinder_val = Input::get('silinder_val');
			$this->silinder_val_ref = Input::get('silinder_val_ref');
			$this->kristal_val = Input::get('kristal_val');
			$this->kristal_val_ref = Input::get('kristal_val_ref');

			$this->result_fisik = Input::get('result_fisik');
			$this->result_hema = Input::get('result_hema');
			$this->result_urine = Input::get('result_urine');
			$this->result_hbsag = Input::get('result_hbsag');
			$this->result_tbc = Input::get('result_tbc');
			$this->result_final = Input::get('result_final');
		}

		public function setValidationData()
		{
			$this->data = array(
				'employee_id' => $this->employee_id,

				'complain' => $this->complain,
	            'medical_history' => $this->medical_history,
	            'weight' => $this->weight,
	            'height' => $this->height,
	            'blood_pressure' => $this->blood_pressure,
	            'is_smoking' => $this->is_smoking == 'on' ? 1 : 0,
	            'is_consuming_alcohol' => $this->is_consuming_alcohol == 'on' ? 1 : 0,
	            'is_exercising' => $this->is_exercising == 'on' ? 1 : 0,
	            'jvp' => $this->jvp == 'on' ? 1 : 0,
	            'apex_beat' => $this->apex_beat == 'on' ? 1 : 0,
	            'cardiac_murmur' => $this->cardiac_murmur == 'on' ? 1 : 0,
	            'murmur' => $this->murmur == 'on' ? 1 : 0,
	            'ronkhi' => $this->ronkhi == 'on' ? 1 : 0,
	            'wheezing' => $this->wheezing == 'on' ? 1 : 0,

	            'hemoglobin_val' => $this->hemoglobin_val,
	            'hemoglobin_ref' => $this->hemoglobin_ref,
	            'leukosit_val' => $this->leukosit_val,
	            'leukosit_ref' => $this->leukosit_ref,
	            'pcv_val' => $this->pcv_val,
	            'pcv_ref' => $this->pcv_ref,
	            'trombosit_val' => $this->trombosit_val,
	            'trombosit_ref' => $this->trombosit_ref,
	            'led_val' => $this->led_val,
	            'led_ref' => $this->led_ref,

	            'hbsag_status' => $this->hbsag_status,
	            'hbsag_reference' => $this->hbsag_reference,
	            'ict_tbc' => $this->ict_tbc,
	            'ict_reference' => $this->ict_reference,

	            'color_val' => $this->color_val,
				'color_val_ref' => $this->color_val_ref,
				'density_val' => $this->density_val,
				'density_val_ref' => $this->density_val_ref,
				'ph_val' => $this->ph_val,
				'ph_val_ref' => $this->ph_val_ref,
				'leukosit_estrase_val' => $this->leukosit_estrase_val,
				'leukosit_estrase_val_ref' => $this->leukosit_estrase_val_ref,
				'nitrit_val' => $this->nitrit_val,
				'nitrit_val_ref' => $this->nitrit_val_ref,
				'protein_val' => $this->protein_val,
				'protein_val_ref' => $this->protein_val_ref,
				'glucose_val' => $this->glucose_val,
				'glucose_val_ref' => $this->glucose_val_ref,
				'keton_val' => $this->keton_val,
				'keton_val_ref' => $this->keton_val_ref,
				'urobilinogen_val' => $this->urobilinogen_val,
				'urobilinogen_val_ref' => $this->urobilinogen_val_ref,
				'bilirubin_val' => $this->bilirubin_val,
				'bilirubin_val_ref' => $this->bilirubin_val_ref,
				'eritrosit_val' => $this->eritrosit_val,
				'eritrosit_val_ref' => $this->eritrosit_val_ref,
				'u_leukosit_val' => $this->u_leukosit_val,
				'u_leukosit_val_ref' => $this->u_leukosit_val_ref,
				'epitel_val' => $this->epitel_val,
				'epitel_val_ref' => $this->epitel_val_ref,
				'bacteria_val' => $this->bacteria_val,
				'bacteria_val_ref' => $this->bacteria_val_ref,
				'silinder_val' => $this->silinder_val,
				'silinder_val_ref' => $this->silinder_val_ref,
				'kristal_val' => $this->kristal_val,
				'kristal_val_ref' => $this->kristal_val_ref,

				'result_fisik' => $this->result_fisik,
				'result_hema' => $this->result_hema,
				'result_urine' => $this->result_urine,
				'result_hbsag' => $this->result_hbsag,
				'result_tbc' => $this->result_tbc,
				'result_final' => $this->result_final,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'employee_id' => 'required',

				'complain' => 'required',
	            'medical_history' => 'required',
	            'weight' => 'required|numeric',
	            'height' => 'required|numeric',
	            'blood_pressure' => 'required',

	            'hemoglobin_val' => 'required|numeric',
	            'hemoglobin_ref' => 'required',
	            'leukosit_val' => 'required|numeric',
	            'leukosit_ref' => 'required',
	            'pcv_val' => 'required|numeric',
	            'pcv_ref' => 'required',
	            'trombosit_val' => 'required|numeric',
	            'trombosit_ref' => 'required',
	            'led_val' => 'required|numeric',
	            'led_ref' => 'required',

	            'hbsag_status' => 'required',
	            'hbsag_reference' => 'required',
	            'ict_tbc' => 'required',
	            'ict_reference' => 'required',

	            'color_val' => 'required',
				'color_val_ref' => 'required',
				'density_val' => 'required|numeric',
				'density_val_ref' =>'required',
				'ph_val' =>'required|numeric',
				'ph_val_ref' => 'required',
				'leukosit_estrase_val' => 'required',
				'leukosit_estrase_val_ref' => 'required',
				'nitrit_val' => 'required',
				'nitrit_val_ref' => 'required',
				'protein_val' => 'required',
				'protein_val_ref' => 'required',
				'glucose_val' => 'required',
				'glucose_val_ref' => 'required',
				'keton_val' => 'required',
				'keton_val_ref' => 'required',
				'urobilinogen_val' => 'required',
				'urobilinogen_val_ref' => 'required',
				'bilirubin_val' => 'required',
				'bilirubin_val_ref' => 'required',
				'eritrosit_val' => 'required',
				'eritrosit_val_ref' => 'required',
				'u_leukosit_val' => 'required',
				'u_leukosit_val_ref' => 'required',
				'epitel_val' => 'required',
				'epitel_val_ref' => 'required',
				'bacteria_val' => 'required',
				'bacteria_val_ref' => 'required',
				'silinder_val' => 'required',
				'silinder_val_ref' => 'required',
				'kristal_val' => 'required',
				'kristal_val_ref' => 'required',

				'result_fisik' => 'required',
				'result_hema' => 'required',
				'result_urine' => 'required',
				'result_hbsag' => 'required',
				'result_tbc' => 'required',
				'result_final' => 'required',
			);
		}
	}