<?php
	
	class EmployeeImportRepository extends BaseRepository
	{
		private $file_import;

		public function getInput()
		{
			$this->file_import = Input::file('file_import');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'file_import' => $this->file_import,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'file_import' => 'required',
			);
		}
	}