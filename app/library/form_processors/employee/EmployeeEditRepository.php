<?php
	
	class EmployeeEditRepository extends BaseRepository
	{
		private $id;
		private $nik;
		private $first_name;
		private $middle_name;
		private $last_name;
		private $age;
		private $gender;
		private $department;

		public function getInput()
		{
			$this->id = Input::get('id');
			$this->nik = Input::get('nik');
			$this->first_name = Input::get('first_name');
			$this->middle_name = Input::get('middle_name');
			$this->last_name = Input::get('last_name');
			$this->age = Input::get('age');
			$this->gender = Input::get('gender');
            $this->department = Input::get('department');
		}

		public function setValidationData()
		{
			$this->data = array(
				'id' => $this->id,
	            'nik' => $this->nik,
	            'first_name' => $this->first_name,
	            'middle_name' => $this->middle_name,
	            'last_name' => $this->last_name,
	            'age' => $this->age,
	            'gender' => $this->gender,
	            'department' => $this->department,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'nik' => 'required|numeric|unique:employees,nik,' . $this->id,
	            'first_name' => 'required',
	            'age' => 'required|numeric'
			);
		}
	}