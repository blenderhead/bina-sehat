<?php
	
	class BsCustomValidator extends \Illuminate\Validation\Validator
	{
		public function validateIsFloat($attribute, $value, $parameters)
		{
			if(!is_float($value))
			{
				return FALSE;
			}

			return TRUE;
		}
	}	
