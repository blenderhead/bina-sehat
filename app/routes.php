<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/test', array(
    'uses' => 'HomeController@getIndex'
));

Route::get('/', array(
    'uses' => 'UserController@getLogin'
));

Route::get('/login', array(
    'as' => 'user.login',
    'uses' => 'UserController@getLogin'
));

Route::post('/login', array(
    'as' => 'user.do.login',
    'uses' => 'UserController@postLogin'
));

Route::get('logout', array(
    'as' => 'user.logout',
    'uses' => 'UserController@getLogout'
));

Route::group(array('before' => 'authorized'), function() {

    Route::group(array('prefix' => 'dashboard'), function() {

        Route::get('/',array(
            'as' => 'dashboard.index',
            'uses' => 'DashboardController@getIndex'
        ));

    });

	Route::group(array('prefix' => 'employee'), function() {

		Route::get('/',array(
            'as' => 'employee.index',
            'uses' => 'EmployeeController@getIndex'
        ));

        Route::get('/get_data', array(
            'as' => 'employee.get_data',
            'uses' => 'EmployeeController@getData'
        ));

        Route::get('/add',array(
            'as' => 'employee.add',
            'uses' => 'EmployeeController@getCreate'
        ));

        Route::post('/add',array(
            'as' => 'employee.create',
            'uses' => 'EmployeeController@postCreate'
        ));

        Route::get('/edit',array(
            'as' => 'employee.edit',
            'uses' => 'EmployeeController@getEdit'
        ));

        Route::post('/edit',array(
            'as' => 'employee.edit',
            'uses' => 'EmployeeController@postEdit'
        ));

        Route::post('/delete',array(
            'as' => 'employee.delete',
            'uses' => 'EmployeeController@postDelete'
        ));

        Route::get('/import',array(
            'as' => 'employee.import',
            'uses' => 'EmployeeController@getImport'
        ));

        Route::post('/import',array(
            'as' => 'employee.import.save',
            'uses' => 'EmployeeController@postImport'
        ));

        Route::get('/api', array(
            'as' => 'employee.api',
            'uses' => 'EmployeeController@getEmployeeDataTables'
        ));
	});

	Route::group(array('prefix' => 'medcheck'), function() {
		
		Route::get('/',array(
            'as' => 'medcheck.index',
            'uses' => 'MedCheckController@getIndex'
        ));

        Route::get('/add',array(
            'as' => 'medcheck.add',
            'uses' => 'MedCheckController@getCreate'
        ));

        Route::post('/add',array(
            'as' => 'medcheck.add.save',
            'uses' => 'MedCheckController@postCreate'
        ));

        Route::get('/edit',array(
            'as' => 'medcheck.edit',
            'uses' => 'MedCheckController@getEdit'
        ));

        Route::post('/edit',array(
            'as' => 'medcheck.edit.save',
            'uses' => 'MedCheckController@postEdit'
        ));

        Route::post('/delete',array(
            'as' => 'medcheck.delete',
            'uses' => 'MedCheckController@postDelete'
        ));

        Route::post('/deletebydate',array(
            'as' => 'medcheck.deletebydate',
            'uses' => 'MedCheckController@postDeleteByDate'
        ));

        Route::get('/printpdf',array(
            'as' => 'medcheck.printpdf',
            'uses' => 'MedCheckController@getPrintPdf'
        ));

        Route::get('/printexcel',array(
            'as' => 'medcheck.printexcel',
            'uses' => 'MedCheckController@getPrintExcel'
        ));

	}); // end medcheck route group

    /**
     * Route Print
     */
    Route::group(array('prefix' => 'print'), function() {
        Route::get('/personal', array(
            'as' => 'print.personal',
            'uses' => 'PrintController@generatePersonal'
        ));
    });
    Route::group(array('prefix' => 'report'), function() {

        Route::get('/master',array(
            'as' => 'report.master',
            'uses' => 'ReportController@getIndex'
        ));

        Route::get('/printexcel',array(
            'as' => 'report.printexcel',
            'uses' => 'ReportController@getPrintExcel'
        ));

    });

});
