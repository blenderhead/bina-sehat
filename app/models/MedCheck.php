<?php

	class MedCheck extends Eloquent 
	{
		protected $table = 'e_checks';

		public function employee()
	    {
	        return $this->belongsTo('Employee');
	    }

	    public function hematology()
	    {
	        return $this->hasOne('Hematology', 'med_check_id', 'id');
	    }

	    public function urine()
	    {
	        return $this->hasOne('UrineCheck', 'med_check_id', 'id');
	    }

	    public function imuno()
	    {
	        return $this->hasOne('ImunoSerology', 'med_check_id', 'id');
	    }

	    public function result()
	    {
	        return $this->hasOne('Result', 'med_check_id', 'id');
	    }

	    public static function getTotalChecked()
	    {
	    	$result = 0;

	    	$med_check = MedCheck::all();

	    	if($med_check)
	    	{
	    		$result = $med_check->count();
	    	}

	    	return $result;
	    }
	}
