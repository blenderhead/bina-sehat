<?php

	class Result extends Eloquent 
	{
		protected $table = 'e_results';

		public function medCheck()
	    {
	        return $this->belongsTo('MedCheck');
	    }
	}
