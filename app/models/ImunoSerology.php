<?php

	class ImunoSerology extends Eloquent 
	{
		protected $table = 'e_imuno';

		public function medCheck()
	    {
	        return $this->belongsTo('MedCheck');
	    }
	}
