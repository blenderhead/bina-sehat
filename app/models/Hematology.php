<?php

	class Hematology extends Eloquent 
	{
		protected $table = 'e_hematologies';

		public function medCheck()
	    {
	        return $this->belongsTo('MedCheck');
	    }
	}
