<?php

	class Department extends Eloquent 
	{
		protected $table = 'departments';

		public function employee()
	    {
	        return $this->belongsTo('Employee');
	    }

	    public static function getDepartmentId($name)
	    {
	    	$id = 1;

	    	$department = Department::where('name',$name)->first();

	    	if($department)
	    	{
	    		$id = $department->id;
	    	}

	    	return $id;
	    }
	}
