<?php

	class Employee extends Eloquent 
	{
		protected $table = 'employees';

		public function dpt()
	    {
	        return $this->hasOne('Department', 'id', 'department');
	    }

		public function medCheck()
	    {
	        return $this->hasMany('MedCheck');
	    }

	    public static function getTotalChecked()
	    {
	    	$result = 0;

	    	$employee_checked = Employee::where('is_check',1)->get();

	    	if(!$employee_checked->isEmpty())
	    	{
	    		$result = $employee_checked->count();
	    	}

	    	return $result;
	    }

	    public static function checkDoubleNik($nik)
	    {
	    	$employee = Employee::where('nik',$nik)->get();

	    	if($employee->isEmpty())
	    	{
	    		return true;
	    	}

	    	return false;
	    }
	}
