<?php

	class DashboardController extends BaseController 
	{
		public function getIndex()
		{
			$data['total_employees'] = Employee::all()->count();
			$data['total_employees_checked'] = MedCheck::getTotalChecked();
			
			return View::make('dashboard.index', $data);
		}
	}
