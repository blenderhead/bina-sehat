<?php

	class ReportController extends BaseController 
	{
		public function getIndex()
		{
			$data['reports'] = MedCheck::groupBy('check_date')->orderBy('check_date','ASC')->get();
            return View::make('report.index', $data);
		}

		public function getPrintExcel()
        {
            //$processor = new AllXlsProcessor();
            //$processor->process();

            $report_date = Input::get('report_date');
            $data['check_date'] = date('j F Y', $report_date);
            $data['reports'] = MedCheck::with('employee','hematology','urine','imuno','result')->where('check_date',date('Y-m-d', $report_date))->get();
            $pdf = PDF::loadView('report.printexcel', $data)->setPaper('a4')->setOrientation('landscape');
            return $pdf->stream();
            //return View::make('medcheck.printexcel');
        }
	}
