<?php

	class EmployeeController extends BaseController 
	{
		public function getIndex()
		{
            //$data['employees'] = Employee::take(10);

            $data['tables'] = Datatable::table()
                              ->addColumn('<input type="checkbox" id="selectall">', 'ID', 'NIK', 'Nama Lengkap', 'Departemen', 'Actions')
                              ->setUrl(route('employee.api'))
                              ->noScript();
                              
            return View::make('employee.index', $data);
		}

        public function getEmployeeDataTables()
        {
            $processor = new EmployeeGetProcessor();
            $processor->process();
            return $processor->getOutput();
        }

		public function getCreate()
		{
            $data['departments'] = Department::orderBy('name')->get();
			return View::make('employee.add', $data);
		}

		public function postCreate()
		{
			$form_processor = new EmployeeAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new EmployeeAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('employee_id', $processor->getOutput()), 200);
            }
		}

		public function getEdit()
		{
            $id = Input::get('id');

            $employee = Employee::find($id);

            if(!$employee)
            {
                return Response::view('errors.404', array(), 404);
            }

            $data['employee'] = $employee;
            $data['departments'] = Department::orderBy('name')->get();
			return View::make('employee.edit', $data);
		}

		public function postEdit()
		{
			$form_processor = new EmployeeEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new EmployeeEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function postDelete()
		{
			$form_processor = new EmployeeDeleteRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new EmployeeDeleteProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

        public function getImport()
        {
            return View::make('employee.import');
        }

        public function postImport()
        {
            $form_processor = new EmployeeImportRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new EmployeeImportProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }
	}
