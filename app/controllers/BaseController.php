<?php

	class BaseController extends Controller 
	{

		public function __construct()
		{
			$credential = Sentry::getUser();

			if($credential)
			{
				$the_user = User::find($credential->id);
				View::share('the_user', $the_user);
			}

			View::share('current_route_name', Route::currentRouteName());
			View::share('credential', $credential);
		}

		protected function setupLayout()
		{
			if ( ! is_null($this->layout))
			{
				$this->layout = View::make($this->layout);
			}
		}

	}

