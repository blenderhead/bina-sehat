<?php

	class MedCheckController extends BaseController 
	{
		public function getCreate()
		{
			$employee_id = Input::get('id');

            $employee = Employee::find($employee_id);

            if(!$employee)
            {
                return Response::view('errors.404', array(), 404);
            }

			$data['employee'] = $employee;

			return View::make('medcheck.add', $data);
		}

		public function postCreate()
		{
			$form_processor = new MedAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new MedAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

        public function getIndex()
        {
            $employee_id = Input::get('id');
            
            $employee = Employee::find($employee_id);

            if(!$employee)
            {
                return Response::view('errors.404', array(), 404);
            }

            $data['employee'] = $employee;
            
            return View::make('medcheck.index', $data);
        }

        public function getEdit()
        {
            $employee_id = Input::get('employee_id');
            $report_id = Input::get('report_id');
            $data['employee'] = Employee::find($employee_id);
            $data['report'] = MedCheck::find($report_id);
            return View::make('medcheck.edit', $data);
        }

        public function postEdit()
        {
            $form_processor = new MedEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new MedEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }

        public function postDelete()
        {
            $form_processor = new MedDeleteRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new MedDeleteProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }

        public function postDeleteByDate()
        {
            $form_processor = new MedDeleteRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new MedDeleteByDateProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }

        public function getPrintPdf()
        {
            $employee_id = Input::get('employee_id');
            $report_id = Input::get('report_id');
            $data['employee'] = Employee::find($employee_id);
            $data['report'] = MedCheck::find($report_id);
            $pdf = PDF::loadView('medcheck.pdfreport', $data);
            return $pdf->stream();
            //return View::make('medcheck.pdfreport', $data);
        }

        public function getPrintExcel()
        {
            //$data['employee_id'] = Input::get('id');
            //$processor = new PersonalXlsProcessor();
            //$processor->process($data);

            $data['employee'] = Employee::find(Input::get('id'));
            $pdf = PDF::loadView('medcheck.printexcel', $data)->setPaper('a4')->setOrientation('landscape');
            return $pdf->stream();
            //return View::make('medcheck.printexcel', $data);
        }
	}
