
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	
	<title>@yield('title')</title>
	
	<!-- BEGIN CORE FRAMEWORK -->
	<link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/ionicons/css/ionicons.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/jqueryui/jquery-ui.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/jqueryui/jquery-ui.theme.css') }}" rel="stylesheet" />
	<!-- END CORE FRAMEWORK -->
	
	<!-- BEGIN PLUGIN STYLES -->
	<link href="{{ asset('assets/plugins/animate/animate.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/bootstrap-slider/css/slider.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/rickshaw/rickshaw.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" />
	<!-- END PLUGIN STYLES -->
	
	<!-- BEGIN THEME STYLES -->
	<link href="{{ asset('assets/css/material.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/plugins.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/helpers.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" />
	<!-- END THEME STYLES -->

	<script type="text/javascript">
		var baseUrl = "{{ URL::to('/') }}";
	</script>

	@yield('styles')

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-leftside fixed-header">
	<!-- BEGIN HEADER -->
	<header>
		<a href="{{ URL::route('dashboard.index') }}" class="logo"><i class="ion-ios-bolt"></i> <span>Admin Dashboard</span></a>
		<nav class="navbar navbar-static-top">
			<a href="#" class="navbar-btn sidebar-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
			
			<!--
            <div class="navbar-right">
				<form role="search" class="navbar-form pull-left" method="post" action="#">
					<div class="btn-inline">
						<input type="text" class="form-control padding-right-35" placeholder="Search..."/>
						<button class="btn btn-link no-shadow bg-transparent no-padding padding-right-10" type="button"><i class="ion-search"></i></button>
					</div>
				</form>
			</div>
			-->
        </nav>
    </header>
	<!-- END HEADER -->
		 
	<div class="wrapper">
		<!-- BEGIN LEFTSIDE -->
        <div class="leftside">
			<div class="sidebar">
				<!-- BEGIN RPOFILE -->
				<div class="nav-profile">
					<div class="thumb">

						@if($credential->image == '')
							<img src="{{ asset('assets/img/avatar.jpg') }}" class="img-circle" alt="" />
						@else
							<img src="{{ URL::to('/') . '/uploads/user/' . $credential->image }}" class="img-circle" alt="" />
						@endif

						<!--<span class="label label-danger label-rounded">3</span>-->
					</div>
					<div class="info">
						<a href="#">{{ $credential->first_name . ' ' . $credential->last_name }}</a>
						<ul class="tools list-inline">
							<li><a href="#" data-toggle="tooltip" title="Settings"><i class="ion-gear-a"></i></a></li>
						</ul>
					</div>
					<a href="{{ URL::route('user.logout') }}" class="button"><i class="ion-log-out"></i></a>
				</div>
				<!-- END RPOFILE -->
				<!-- BEGIN NAV -->
				<div class="title">Menu</div>
					<ul class="nav-sidebar">
						<li @if($current_route_name == 'dashboard.index'){{'class="active"'}}@endif>
                            <a href="{{ URL::route('dashboard.index') }}">
                                <i class="ion-home"></i> <span>Home</span>
                            </a>
                        </li>

                        <!--
                        <li class="nav-dropdown @if($current_route_name == 'employee.index' || $current_route_name == 'employee.add'){{'active open'}}@endif">
                            <a href="#">
                                <i class="ion-person-stalker"></i> <span>Employee Management</span>
                                <i class="ion-chevron-right pull-right"></i>
                            </a>
                            <ul>
                                <li @if($current_route_name == 'employee.index'){{'class="active"'}}@endif><a href="{{ URL::route('employee.index') }}">Employee List</a></li>
                                <li @if($current_route_name == 'employee.add'){{'class="active"'}}@endif><a href="{{ URL::route('employee.add') }}">Add Employee</a></li>
                            </ul>
                        </li>
                    	-->
                        
                        <li @if($current_route_name == 'employee.index'){{'class="active"'}}@endif>
                            <a href="{{ URL::route('employee.index') }}">
                                <i class="ion-document-text"></i> <span>Daftar Karyawan</span>
                            </a>
                        </li>

                        <li @if($current_route_name == 'report.master'){{'class="active"'}}@endif>
                            <a href="{{ URL::route('report.master') }}">
                                <i class="ion-document-text"></i> <span>Lihat Semua Laporan</span>
                            </a>
                        </li>

                    </ul>
					<!-- END NAV -->
					
					
			</div><!-- /.sidebar -->
        </div>
		<!-- END LEFTSIDE -->

		<!-- BEGIN RIGHTSIDE -->
        <div class="rightside bg-grey-100">
			<!-- BEGIN PAGE HEADING -->
            <div class="page-head bg-grey-100">
				<h1 class="page-title">@yield('page_title')<small>@yield('page_description')</small></h1>
			</div>
			<!-- END PAGE HEADING -->

			@yield('user_information')
			
            <div class="container-fluid">
				@yield('content')
            </div><!-- /.container-fluid -->
        </div><!-- /.rightside -->
    </div><!-- /.wrapper -->
	<!-- END CONTENT -->
		
	<!-- BEGIN JAVASCRIPTS -->
	
	<!-- BEGIN CORE PLUGINS -->
	<script src="{{ asset('assets/plugins/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/bootstrap/js/holder.js') }}"></script>
	<script src="{{ asset('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/jqueryui/jquery-ui.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/core.js" type="text/javascript') }}"></script>
	<!-- END CORE PLUGINS -->
	
	<!-- flot chart -->
	<!--
	<script src="{{ asset('assets/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/flot/jquery.flot.grow.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
	-->
	
	<!-- sparkline -->
	<script src="{{ asset('assets/plugins/sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
	
	<!-- bootstrap slider -->
	<script src="{{ asset('assets/plugins/bootstrap-slider/js/bootstrap-slider.js') }}" type="text/javascript"></script>
	
	<!-- datepicker -->
	<script src="{{ asset('assets/plugins/bootstrap-daterangepicker/moment.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
	
	<!-- vectormap -->
	<script src="{{ asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-europe-merc-en.js') }}" type="text/javascript"></script>
	
	<!-- counter -->
	<script src="{{ asset('assets/plugins/jquery-countTo/jquery.countTo.js') }}" type="text/javascript"></script>
	
	<!-- rickshaw -->
	<script src="{{ asset('assets/plugins/rickshaw/vendor/d3.v3.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/rickshaw/rickshaw.min.js') }}" type="text/javascript"></script>

	<!-- ion-rangeSlider -->
	<script src="{{ asset('assets/plugins/ion-rangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js') }}"></script>
	
	<!-- knob -->
	<script src="{{ asset('assets/plugins/jquery-knob/jquery.knob.min.js') }}" type="text/javascript"></script>

	<!-- bootstrap validator -->
	<script src="{{ asset('assets/plugins/bootstrapValidator/bootstrapValidator.min.js') }}" type="text/javascript"></script>

	<!-- input mask -->
    <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.numeric.extensions.js') }}" type="text/javascript"></script>
	
	<!-- switchery -->
    <script src="{{ asset('assets/plugins/switchery/switchery.min.js') }}" type="text/javascript"></script>
	
	<!-- datepicker -->
    <script src="{{ asset('assets/plugins/datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
	
	<!-- colorpicker -->
    <script src="{{ asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}" type="text/javascript"></script>
	
	<!-- bootstrap select -->
    <script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
	
	<!-- iCheck -->
    <script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/plugins/waitme/waitMe.js') }}" type="text/javascript"></script>
	
	<!-- maniac -->
	<script src="{{ asset('assets/js/maniac.js') }}" type="text/javascript"></script>

	<script src="{{ asset('assets/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>

	<script src="{{ asset('assets/js/functions.js') }}" type="text/javascript"></script>
	
	<!-- dashboard -->
	<script type="text/javascript">
		//maniac.loadchart();
		maniac.loadvectormap();
		maniac.loadbsslider();
		//maniac.loadrickshaw();
		maniac.loadcounter();
		maniac.loadprogress();
		maniac.loaddaterangepicker();
		showOpTooltip();
	</script> 

	@yield('scripts')

	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>