<!DOCTYPE html>
<html>
<head>
	<title>Print out MEDICAL CHECK UP</title>
</head>
<body>
<style type="text/css">
/*css belum dipisah */
.header,
.footer,{
    width: 100%;
    text-align: center;
    position: fixed;
}
.header {
    top: 0px;
}
.footer {
    bottom: 0px;
}
.pagenum:before {
    content: counter(page);
}
.header-cover,.header-2 {
	text-align: center;
}
</style>
<div class="header-cover">
    <!-- Page <span class="pagenum"></span> -->
    <h3>HASIL PEMERIKSAAN MEDICAL CHECK UP<br> KARYAWAN / KARYAWATI<br>PT. KALDU SARI NABATI</h3>
    &check;
    &#10004;
    &#x2714;
</div><hr><br><br>
<table style="margin-left: 0px; margin-right: auto">
 <tr>
 	<td>NO</td>
 	<td>:&nbsp;{{$employee->id}}</td>
 </tr>
 <tr>
 	<td>TANGGAL</td>
 	<td>:&nbsp;{{date('D F Y', strtotime($employee->medCheck[0]->check_date))}}</td>
 </tr>
 <tr>
 	<td>NAMA</td>
 	<td>:&nbsp;{{$employee->first_name.' '.$employee->last_name}}</td>
 </tr>
 <tr>
 	<td>USIA/KELAMIN</td>
 	<td>:&nbsp;{{$employee->age.' Th / '.$employee->gender}}</td>
 </tr>
 <tr>
 	<td>NIK</td>
 	<td>:&nbsp;{{$employee->nik}}</td>
 </tr>
 <tr>
 	<td>BAGIAN</td>
 	<td>:&nbsp;{{$employee->dpt->name}}</td>
 </tr>
</table>
<div class="footer">
    Page <span class="pagenum"></span>
</div>
<div style="page-break-before: always;"></div>
<!-- end cover -->

<!--page 2--><br>
<div class="header-2">
	<table width="600">
	  <tr>
	    <td>
			<table>
				<tr>
				 	<td>NO </td>
				 	<td>:&nbsp;{{$employee->id}}</td>
				</tr>
				<tr>
				 	<td>TANGGAL</td>
				 	<td>:&nbsp;{{date('D F Y', strtotime($employee->medCheck[0]->check_date))}}</td>
				 </tr>
				<tr>
				 	<td>NAMA</td>
				 	<td>:&nbsp;{{$employee->first_name.' '.$employee->last_name}}</td>
				</tr>
				<tr>
				 	<td>USIA/KELAMIN</td>
				 	<td>:&nbsp;{{$employee->age.' Tahun / '}} {{($employee->gender == 'l') ? 'Laki-laki' : 'Perempuan'}}</td>
				</tr>
			</table>
		</td>
		<td>
			<table>
				<tr>
				 	<td>NIK</td>
				 	<td>:&nbsp;{{$employee->nik}}</td>
				 </tr>
				 <tr>
				 	<td>Telp</td>
				 	<td>:&nbsp;-</td>
				 </tr>
				 <tr>
				 	<td>BAGIAN</td>
				 	<td>:&nbsp;{{$employee->dpt->name}}</td>
				 </tr>
				 <tr>
				 	<td>&nbsp;</td>
				 	<td>&nbsp;</td>
				 </tr>

			</table>
		</td>
	  </tr>
	</table>
</div>
<hr>
<div class="header-content">
	<center><u><h4>PEMERIKSAAN FISIK</h4></u></center>
</div><br><br>
<div class="content">
	<div class="table-1">
		<table>
			<tr>
				<td><b>Keluhan Saat Ini</b></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>:&nbsp;<b>Tidak Ada Keluhan</b></td>
			</tr>
			<tr>
				<td><b>Riwayat Penyakit</b></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>:&nbsp;<b>Tidak Ada</b></td>
			</tr>
		</table>
	</div><br><br>
	<div class="table-2">
		<table>
			<tr>
				<td><b>Gaya Hidup</b></td>
			</tr>
			<tr>
				<td><b>Jenis Kebiasaan</b></td>
			</tr>
		</table>
		<table>
			<tr>
				<td>
					<table style="margin-right:40px;">
						<tr>
							<td>Merokok</td>
						</tr>
						<tr>
							<td>Minum Alkohol</td>
						</tr>
						<tr>
							<td>Olah Raga</td>
						</tr>
					</table>
				</td>
				<td>
					<table border="1" style="margin-right:20px;">
						<tr>
							<td>asdf</td>
						</tr>
						<tr>
							<td>asdf</td>
						</tr>
						<tr>
							<td>asdf</td>
						</tr>
					</table>
				</td>
				<td>
					<table border="1">
						<tr>
							<td>asdf</td>
						</tr>
						<tr>
							<td>asdf</td>
						</tr>
						<tr>
							<td>asdf</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div><br><br>
	<div class="table-3">
		<table>
			<tr>
				<td>
					<table style="margin-right:200px;">
						<tr>
							<td><b>Tanda-tanda Vital</b></td>
						</tr>
						<tr>
							<td>Tekanan Darah</td>
						</tr>
						<tr>
							<td>Tinggi Badan</td>
						</tr>
						<tr>
							<td>Berat Badan</td>
						</tr>
					</table>
				</td>
				<td>
					<table>
						<tr>
							<td><b>Tanda-tanda Vital</b></td>
						</tr>
						<tr>
							<td>120/80 mmHg</td>
						</tr>
						<tr>
							<td>170 Cm</td>
						</tr>
						<tr>
							<td>50 Kg</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div><br><br>
	<div class="table-4">
		<table>
			<tr>
				<td>
					<table style="margin-right:50px;">
						<tr>
							<td>JVP</td>
						</tr>
						<tr>
							<td>Apex Beat</td>
						</tr>
						<tr>
							<td>Bising Jantung</td>
						</tr>
						<tr>
							<td>Murumur</td>
						</tr>
						<tr>
							<td>Roukhi</td>
						</tr>
						<tr>
							<td>Wheezing</td>
						</tr>
					</table>
				</td>
				<td>
					<table border="1" style="margin-right:20px">
						<tr>
							<td>asdf</td>
						</tr>
						<tr>
							<td>asdf</td>
						</tr>
						<tr>
							<td>asdf</td>
						</tr>
						<tr>
							<td>asdf</td>
						</tr>
						<tr>
							<td>asdf</td>
						</tr>
						<tr>
							<td>asdf</td>
						</tr>
					</table>
				</td>
				<td>
					<table border="1">
						<tr>
							<td>asdf</td>
						</tr>
						<tr>
							<td>asdf</td>
						</tr>
						<tr>
							<td>asdf</td>
						</tr>
						<tr>
							<td>asdf</td>
						</tr>
						<tr>
							<td>asdf</td>
						</tr>
						<tr>
							<td>asdf</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</div>


</body>
</html>