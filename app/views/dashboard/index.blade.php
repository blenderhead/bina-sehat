@extends('layouts.backend')

@section('title')
	Dashboard - Index
@stop

@section('page_title')
	Bina Sehat Medical Check Management System
@stop

@section('page_description')
	selamat datang di halaman muka administrasi
@stop

@section('content')
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="panel bg-teal-500">
				<div class="panel-body padding-15-20">
					<div class="clearfix">
						<div class="pull-left">
							<div class="color-white font-size-26 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="{{ $total_employees }}" data-speed="500" data-refresh-interval="10"></div>
							<div class="display-block color-teal-50 font-weight-600"><!--<i class="ion-plus-round"></i>--> KARYAWAN TERDAFTAR</div>
						</div>
						<div class="pull-right">
							<i class="font-size-36 color-teal-100 ion-person-stalker"></i>
						</div>
					</div>
					<!--
					<div class="progress progress-animation progress-xs margin-top-25 margin-bottom-5">
						<div class="progress-bar bg-teal-100" role="progressbar" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
						</div>
					</div>
					<div class="font-size-11 clearfix color-teal-50 font-weight-600">
						<div class="pull-left">PROGRESS</div>
						<div class="pull-right">72%</div>
					</div>
					-->
				</div>
			</div><!-- /.panel -->
		</div><!-- /.col -->
								
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="panel bg-red-400">
				<div class="panel-body padding-15-20">
					<div class="clearfix">
						<div class="pull-left">
							<div class="color-white font-size-26 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="{{ $total_employees_checked }}" data-speed="500" data-refresh-interval="10"></div>
							<div class="display-block color-red-50 font-weight-600"><!--<i class="ion-plus-round"></i>--> TELAH DIPERIKSA</div>
						</div>
						<div class="pull-right">
							<i class="font-size-36 color-red-100 ion-medkit"></i>
						</div>
					</div>
					<!--
					<div class="progress progress-animation progress-xs margin-top-25 margin-bottom-5">
						<div class="progress-bar bg-red-100" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
						</div>
					</div>
					<div class="font-size-11 clearfix color-red-50 font-weight-600">
						<div class="pull-left">UNREAD</div>
						<div class="pull-right">80%</div>
					</div>
					-->
				</div>
			</div><!-- /.panel -->
		</div><!-- /.col -->

	</div><!-- /.row -->
@stop

@section('after-twit-nav')

@stop
@section('scripts')

@stop