<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Daftar Report</title>
        <link href="{{ asset('assets/plugins/bootstrap2/css/bootstrap.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/pdf-style.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/pdf.css') }}" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('assets/css/excel.css') }}" />
    </head>

    <body>

        <div class="container">

            <div class="row">
                <div class="span10 offset1 text-center padding-bottom-10">
                    <h5>DATA HASIL LABORATORIUM</h5>
                    <h5>MEDICAL CHECKUP</h5>
                    <h5>PT. KALDU SARI NABATI</h5>
                    <h5>Tanggal periksa: {{ $check_date }}</h5>
                </div>
            </div>
                
        </div>
        
        <table id="travel">

        	<thead> 
            	<tr>
                    <th scope="col" rowspan="2">No</th>
                    <th scope="col" rowspan="2">Nik</th>
                    <th scope="col" rowspan="2">Nama Peserta</th>
                    <th scope="col" rowspan="2">Usia</th>
                    <th scope="col" rowspan="2">L/P</th>
                    <th scope="col" rowspan="2">Bagian</th>

                    <th scope="col" colspan="6">Pemeriksaan Fisik</th>
                    <th scope="col" colspan="6">Hematologi Rutin</th>
                    <th scope="col" colspan="8">Urine Rutin</th>
                </tr>
                
                <tr>
                    <th scope="col" width="5%">Merokok</th>
                    <th scope="col" width="5%">Alkohol</th>
                    <th scope="col" width="5%">Olah Raga</th>
                    <th scope="col">Tensi</th>
                    <th scope="col">BB</th>
                    <th scope="col">TB</th>

                    <th scope="col">HB</th>
                    <th scope="col">Leu</th>
                    <th scope="col">PCV</th>
                    <th scope="col">Trombisit</th>
                    <th scope="col">LED</th>
                    <th scope="col">HbsAg</th>

                    <th scope="col">Wrn</th>
                    <th scope="col">pH</th>
                    <th scope="col">BJ</th>
                    <th scope="col">Lain-lain</th>
                    <th scope="col">Epi</th>
                    <th scope="col">Leu</th>
                    <th scope="col">Eri</th>
                    <th scope="col">Bak</th>
                </tr>  
           
            </thead>
            
            <tbody>

                <?php $iterator = 1; ?>

                @foreach($reports as $report)
                	<tr>
                		<th scope="row">{{ $iterator }}</th>
                        <td>{{ $report->employee->nik }}</td>
                        <td>{{ $report->employee->first_name . ' ' . $report->employee->middle_name . ' ' . $report->employee->last_name }}</td>
                        <td>{{ $report->employee->age }}</td>
                        <td>{{ strtoupper($report->employee->gender) }}</td>
                        <td>{{ $report->employee->dpt->name }}</td>

                        <td>{{ $report->is_smoking ? 'Y' : 'T' }}</td>
                        <td>{{ $report->is_consuming_alcohol ? 'Y' : 'T' }}</td>
                        <td>{{ $report->is_exercising ? 'Y' : 'T' }}</td>
                        <td>{{ $report->blood_pressure }}</td>
                        <td>{{ $report->weight }}</td>
                        <td>{{ $report->height }}</td>

                        <td>{{ $report->hematology->hemoglobin_val }}</td>
                        <td>{{ $report->hematology->leukosit_val }}</td>
                        <td>{{ $report->hematology->pcv_val }}</td>
                        <td>{{ $report->hematology->trombosit_val }}</td>
                        <td>{{ $report->hematology->led_val }}</td>
                        <td>{{ Helper::processPolar($report->imuno->hbsag_status) }}</td>

                        <td>{{ strtoupper($report->urine->color_val) }}</td>
                        <td>{{ $report->urine->ph_val }}</td>
                        <td>{{ $report->urine->density_val }}</td>
                        <td>{{ Helper::processPolar($report->urine->protein_val) }}</td>
                        <td>{{ $report->urine->epitel_val }}</td>
                        <td>{{ $report->urine->leukosit_val }}</td>
                        <td>{{ $report->urine->eritrosit_val }}</td>
                        <td>{{ Helper::processPolar($report->urine->bacteria_val) }}</td>
                    </tr>

                    <?php $iterator++; ?>
                @endforeach

            </tbody>

        </table>

    </body>
</html>