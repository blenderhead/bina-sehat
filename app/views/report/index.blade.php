@extends('layouts.backend')

@section('title')
	Master Report - Index
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Master Report
@stop

@section('page_description')
	all medical check list
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

                <div class="panel-title bg-white no-border">
                	<div class="panel-tools float-left add-bottom-20">
						<a href="" class="delete-all"><i class="fa fa-remove"></i> Hapus Laporan</a>
					</div>
				</div>

                <div class="panel-body no-padding-top bg-white">

					<table id="example1" class="table table-bordered table-striped">

						<thead>
							<tr>
								<th width="5"><input type="checkbox" id="selectall"></th>
								<th>Tanggal Periksa</th>
								<th>Actions</th>
							</tr>
						</thead>
						
						<tbody>
							@foreach($reports as $report)
								<tr>
									<td width="5"><input type="checkbox" class="report" value="{{ $report->check_date }}"></td>
									<td>{{ $report->check_date }}</td>
									<td>
										<button title="Hapus laporan" data-toggle="tooltip" type="button" class="btn btn-warning btn-circle delete" data-report-id="{{ $report->check_date }}"><i class="glyphicon glyphicon-remove"></i></button>
										<a target="_blank" title="Print laporan" target="_blank" data-toggle="tooltip" href="{{ URL::to('/') . '/report/printexcel?report_date=' . strtotime($report->check_date) }}" type="button" class="btn btn-success btn-circle edit"><i class="glyphicon glyphicon-print"></i></a>
									</td>
								</tr>
							@endforeach
						</tbody>
						
						<tfoot>
							<tr>
								<th></th>
								<th>Tanggal Periksa</th>
								<th>Actions</th>
							</tr>
						</tfoot>

					</table>

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<!-- datatables -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
	</script>

	<script src="{{ asset('assets/js/classes/ReportManager.js') }}" type="text/javascript"></script>
@stop