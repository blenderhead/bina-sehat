@extends('layouts.backend')

@section('title')
	Karyawan - Import Data
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Import Employee
@stop

@section('page_description')
	import data karyawan
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="import-employee" method="post">	

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="author_image" class="col-lg-2 control-label">Pilih file</label>
							<div class="col-lg-8">
								<input type="file" class="file_import" id="file_import" name="file_import">
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Import</button>
							<a href="{{ URL::route('employee.index') }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')

    <script src="{{ asset('assets/js/classes/employee/Employee.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/classes/errors/employee_errors.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/EmployeeManager.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {

			$(".employee_gender, .employee_dpt").selectpicker();

		});
	</script>
	
@stop