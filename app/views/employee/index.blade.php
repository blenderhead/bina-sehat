@extends('layouts.backend')

@section('title')
	Karyawan - Index
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Karyawan
@stop

@section('page_description')
	daftar karyawan
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

                <div class="panel-title bg-white no-border">
                	<div class="panel-tools float-left add-bottom-20">
						<a href="{{ URL::route('employee.add') }}"><i class="fa fa-user"></i> Tambah Karyawan</a>
						<a href="" class="delete-all"><i class="fa fa-remove"></i> Hapus Karyawan</a>
						<a href="{{ URL::route('employee.import') }}" class=""><i class="fa fa-file-excel-o"></i> Import Data</a>
					</div>
				</div>

                <div class="panel-body no-padding-top bg-white member_table_wrapper">

					{{ $tables->render() }}

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<!-- datatables -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">

		$(document).ready(function() {

			$('.member_table_wrapper table').dataTable({
		        "sPaginationType": "bootstrap",
		        "bProcessing": false,
		        "sAjaxSource": baseUrl + '/employee/api',
		        "bServerSide": true,
		        "aoColumns": [
		          { "bSortable": false },
		          null,
		          null,
		          null,
		          null,
		          null
		        ]
		    });

		});

	</script>

	<script src="{{ asset('assets/js/classes/employee/Employee.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/EmployeeManager.js') }}" type="text/javascript"></script>
@stop