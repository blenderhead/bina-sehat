@extends('layouts.backend')

@section('title')
	Employee - Edit
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Edit Employee
@stop

@section('page_description')
	edit employee form
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="edit-employee" method="post" data-op="edit">		

						<div class="form-group no-margin-left no-margin-right">
							<label for="name" class="col-lg-2 control-label"><span class="required">*</span> <span class="required-info">harus diisi</span></label>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="employee_nik" class="col-lg-2 control-label">NIK <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control employee_nik" id="employee_nik" placeholder="employee ID number" name="nik" value="{{ $employee->nik }}">
								<span class="nik-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>
							
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="employee_first_name" class="col-lg-2 control-label">Nama Depan <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control employee_first_name" id="employee_first_name" name="first_name" value="{{ $employee->first_name }}">
								<span class="firts-name-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="employee_middle_name" class="col-lg-2 control-label">Name Tengah</label>
							<div class="col-lg-8">
								<input type="text" class="form-control employee_middle_name" id="employee_middle_name" name="middle_name" value="{{ $employee->middle_name }}">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="employee_last_name" class="col-lg-2 control-label">Nama Belakang</label>
							<div class="col-lg-8">
								<input type="text" class="form-control employee_last_name" id="employee_last_name" name="last_name" value="{{ $employee->last_name }}">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="employee_age" class="col-lg-2 control-label">Umur <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control employee_age" id="employee_age" name="age" value="{{ $employee->age }}">
								<span class="age-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="employee_dpt" class="col-lg-2 control-label">Jenis Kelamin</label>
							<div class="col-lg-8">
								<select class="employee_dpt" name="gender">
									<option value="l" @if($employee->gender == 'l'){{'selected'}}@endif>Laki-laki</option>
									<option value="p" @if($employee->gender == 'p'){{'selected'}}@endif>Perempuan</option>
								</select>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="employee_department" class="col-lg-2 control-label">Departemen</label>
							<div class="col-lg-8">
								<select class="employee_gender" name="department">
									@foreach($departments as $department)
										<option value="{{ $department->id }}" @if($employee->department == $department->id){{ 'selected' }}@endif>{{ $department->name }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<input type="hidden" name="id" value="{{ $employee->id }}" class="employee_id" />
							<a href="{{ URL::to('/') . '/employee' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Save</button>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')

    <script src="{{ asset('assets/js/classes/employee/Employee.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/classes/errors/employee_errors.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/EmployeeManager.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {

			$(".employee_gender, .employee_dpt").selectpicker();

		});
	</script>

@stop