<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<meta content="" name="description"/>
		<meta content="" name="author"/>
		
		<title>Report</title>

		<link href="{{ asset('assets/plugins/bootstrap2/css/bootstrap.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('assets/css/pdf-style.css') }}" rel="stylesheet" />
		<link href="{{ asset('assets/css/pdf.css') }}" rel="stylesheet" />

	</head>

	<body>
		<div class="container">

			<div class="logo-bs">
				<img src="{{ asset('assets/img/logo-bs.png') }}">
			</div>

			<div class="page-one">
				<div class="row">
			     	<div class="span10 offset1 text-center doubleUnderline">
			     		<h4>HASIL PEMERIKSAAN MEDICAL CHECKUP</h3>
			     		<h4>KARYAWAN / KARYAWATI</h3>
			     		<h4>PT. KALDU SARI NABATI</h3>
					</div>
				</div>

				<div class="row">
					<div class="span10 offset4 text-center">
			     		<table class="table employee-info">
						    <tbody>
						      <tr>
						        <td width="20">Tanggal</td>
						        <td width="20">:</td>
						        <td>{{ date('d F Y', strtotime($report->check_date)) }}</td>
						      </tr>
						      <tr>
						        <td width="20">Nama</td>
						        <td width="20">:</td>
						        <td>{{ ucwords($employee->first_name . ' ' . $employee->middle_name . ' ' . $employee->last_name)  }}</td>
						      </tr>
						      <tr>
						        <td width="20">Usia/Kelamin</td>
						        <td width="20">:</td>
						        <td>{{ $employee->age }} / {{ Helper::processGender($employee->gender) }}</td>
						      </tr>
						      <tr>
						        <td width="20">NIK</td>
						        <td width="20">:</td>
						        <td>{{ $employee->nik }}</td>
						      </tr>
						      <tr>
						        <td width="20">Bagian</td>
						        <td width="20">:</td>
						        <td>{{ $employee->dpt->name }}</td>
						      </tr>
						    </tbody>
						  </table>
					</div>
			    </div>
			</div>
			
			<div class="page-two">

				<div class="logo-bs">
					<img src="{{ asset('assets/img/logo-bs.png') }}">
				</div>

				<div class="row">
					<div class="span10 offset1 doubleUnderline">
			     		<div class="span5"><p><strong>Tanggal : {{ date('d F Y', strtotime($report->check_date)) }}</strong></p></div>
  						<div class="span4"><p><strong>Bagian : {{ $employee->dpt->name }}</p></strong></div>
  						<div class="span5"><p><strong>Nama : {{ ucwords($employee->first_name . ' ' . $employee->middle_name . ' ' . $employee->last_name)  }}</p></strong></div>
  						<div class="span4"><p><strong>Telp : {{ $employee->phone ? $employee->phone : 'N/A' }}</p></strong></div>
  						<div class="span5"><p><strong>Usia/Kelamin : {{ $employee->age }} / {{ Helper::processGender($employee->gender) }}</p></strong></div>
  						<div class="span4"><p><strong>NIK : {{ $employee->nik }}</strong></p></div>
					</div>
			     	
			     	<div class="row">
				     	<div class="span10 offset1 text-center padding-top-10 padding-bottom-10">
				     		<h4>PEMERIKSAAN FISIK</h4>
						</div>
					</div>

					<div class="row">
						<div class="span10 offset1 text-center">
				     		<table class="table">
							    <tbody>
							      <tr>
							        <td width="20%"><strong>Keluhan Saat Ini</strong></td>
							        <td width="2%"> :</td>
							        <td><strong>{{ $report->complain }}</strong></td>
							      </tr>
							      <tr>
							        <td width="20%"><strong>Riwayat Penyakit</strong></td>
							        <td width="2%">:</td>
							        <td><strong>{{ $report->medical_history }}</strong></td>
							      </tr>
							    </tbody>
							  </table>

							  <br />
							  <p style="text-align: left !important; left: 12px !important; position: relative;"><strong>Gaya Hidup</strong></p>

							  <table class="table">
							    <tbody>
							      <tr>
							        <td width="25%"><strong>Jenis Kebiasaan</strong></td>
							        <td width="7%" style="text-align: center !important"><strong>Y</strong></td>
							        <td width="7%"></td>
							        <td width="7%"></td>
							        <td width="7%" style="text-align: center !important"><strong>T</strong></td>
							      </tr>
							      <tr>
							        <td width="21%">Merokok</td>
							        <td width="7%" class="outline" style="text-align: center !important;">{{ $report->is_smoking ? 'v' : '' }}</td>
							        <td width="7%"></td>
							        <td width="7%"></td>
							        <td class="outline" style="text-align: center !important;">{{ !$report->is_smoking ? 'v' : '' }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Minum Alkohol</td>
							        <td width="7%" class="outline" style="text-align: center !important;">{{ $report->is_consuming_alcohol ? 'v' : '' }}</td>
							        <td width="7%"></td>
							        <td width="7%"></td>
							        <td class="outline" style="text-align: center !important;">{{ !$report->is_consuming_alcohol ? 'v' : '' }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Olah Raga</td>
							        <td class="outline" style="text-align: center !important;">{{ $report->is_exercising ? 'v' : '' }}</td>
							        <td width="7%"></td>
							        <td width="7%"></td>
							        <td class="outline" style="text-align: center !important;">{{ !$report->is_exercising ? 'v' : '' }}</td>
							      </tr>
							    </tbody>
							  </table>

							  <br />

							  <table class="table">
							    <tbody>
							      <tr>
							      	<td width="20%"><strong>Tanda-tanda Vital</strong></td>
							        <td></td>
							        <td><strong>Hasil/Jenis</strong></td>
							      </tr>
							      <tr>
							        <td width="20%">Tekanan Darah</td>
							        <td> :</td>
							        <td>{{ $report->blood_pressure }}</td>
							      </tr>
							      <tr>
							        <td width="20%">Tinggi Badan</td>
							        <td>:</td>
							        <td>{{ $report->height }} Cm</td>
							      </tr>
							      <tr>
							        <td width="20%">Berat adan</td>
							        <td>:</td>
							        <td>{{ $report->weight }} Kg</td>
							      </tr>
							    </tbody>
							  </table>

							  <table class="table">
							    <tbody>
							      <tr>
							        <td width="25%"></td>
							        <td width="7%" style="text-align: center !important"><strong>Y</strong></td>
							        <td width="7%"></td>
							        <td width="7%"></td>
							        <td width="7%" style="text-align: center !important"><strong>T</strong></td>
							      </tr>
							      <tr>
							        <td width="21%">JVP</td>
							        <td width="7%" class="outline" style="text-align: center !important;">{{ $report->jvp ? 'v' : '' }}</td>
							        <td width="7%"></td>
							        <td width="7%"></td>
							        <td class="outline" style="text-align: center !important;">{{ !$report->jvp ? 'v' : '' }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Apex Beat</td>
							        <td width="7%" class="outline" style="text-align: center !important;">{{ $report->apex_beat ? 'v' : '' }}</td>
							        <td width="7%"></td>
							        <td width="7%"></td>
							        <td class="outline" style="text-align: center !important;">{{ !$report->apex_beat ? 'v' : '' }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Bising Jantung</td>
							        <td class="outline" style="text-align: center !important;">{{ $report->cardiac_murmur ? 'v' : '' }}</td>
							        <td width="7%"></td>
							        <td width="7%"></td>
							        <td class="outline" style="text-align: center !important;">{{ !$report->cardiac_murmur ? 'v' : '' }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Murmur</td>
							        <td class="outline" style="text-align: center !important;">{{ $report->murmur ? 'v' : '' }}</td>
							        <td width="7%"></td>
							        <td width="7%"></td>
							        <td class="outline" style="text-align: center !important;">{{ !$report->murmur ? 'v' : '' }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Ronkhi</td>
							        <td class="outline" style="text-align: center !important;">{{ $report->ronkhi ? 'v' : '' }}</td>
							        <td width="7%"></td>
							        <td width="7%"></td>
							        <td class="outline" style="text-align: center !important;">{{ !$report->ronkhi ? 'v' : '' }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Wheezing</td>
							        <td class="outline" style="text-align: center !important;">{{ $report->wheezing ? 'v' : '' }}</td>
							        <td width="7%"></td>
							        <td width="7%"></td>
							        <td class="outline" style="text-align: center !important;">{{ !$report->wheezing ? 'v' : '' }}</td>
							      </tr>
							    </tbody>
							  </table>
						</div>
					</div>

				</div>
			</div>

			<div class="page-three">	

				<div class="logo-bs">
					<img src="{{ asset('assets/img/logo-bs.png') }}">
				</div>

				<div class="row">
					<div class="span10 offset1 doubleUnderline">
			     		<div class="span5"><p><strong>Tanggal : {{ date('d F Y', strtotime($report->check_date)) }}</strong></p></div>
  						<div class="span4"><p><strong>Bagian : {{ $employee->dpt->name }}</p></strong></div>
  						<div class="span5"><p><strong>Nama : {{ ucwords($employee->first_name . ' ' . $employee->middle_name . ' ' . $employee->last_name)  }}</p></strong></div>
  						<div class="span4"><p><strong>Telp : {{ $employee->phone ? $employee->phone : 'N/A' }}</p></strong></div>
  						<div class="span5"><p><strong>Usia/Kelamin : {{ $employee->age }} / {{ Helper::processGender($employee->gender) }}</p></strong></div>
  						<div class="span4"><p><strong>NIK : {{ $employee->nik }}</strong></p></div>
					</div>
			     	
			     	<div class="row">
				     	<div class="span10 offset1 text-center padding-top-10 padding-bottom-10">
				     		<h4>HASIL PEMERIKSAAN LABORATORIUM</h4>
						</div>
					</div>

					<div class="row">
						<div class="span10 offset1 text-center">

							  <table class="table">
							    <tbody>
							      <tr>
							        <td width="25%"><strong>Hematologi Rutin</strong></td>
							        <td style="text-align: center !important"><strong>Hasil</strong></td>
							        <td style="text-align: center !important"><strong>Hasil Rujukan</strong></td>
							      </tr>
							      <tr>
							        <td width="25%">Hb</td>
							        <td style="text-align: center !important;">{{ $report->hematology->hemoglobin_val }}</td>
							        <td style="text-align: center !important;">{{ $report->hematology->hemoglobin_ref }}</td>
							      </tr>
							      <tr>
							        <td width="25%">Lekosit</td>
							        <td style="text-align: center !important;">{{ $report->hematology->leukosit_val }}</td>
							        <td style="text-align: center !important;">{{ $report->hematology->leukosit_ref }}</td>
							      </tr>
							      <tr>
							        <td width="25%">Hematokrit</td>
							        <td style="text-align: center !important;">{{ $report->hematology->pcv_val }}</td>
							        <td style="text-align: center !important;">{{ $report->hematology->pcv_ref }}</td>
							      </tr>
							      <tr>
							        <td width="25%">Trombosit</td>
							        <td style="text-align: center !important;">{{ $report->hematology->trombosit_val }}</td>
							        <td style="text-align: center !important;">{{ $report->hematology->trombosit_ref }}</td>
							      </tr>
							      <tr>
							        <td width="25%">LED</td>
							        <td style="text-align: center !important;">{{ $report->hematology->led_val }}</td>
							        <td style="text-align: center !important;">{{ $report->hematology->led_ref }}</td>
							      </tr>
							    </tbody>
							  </table>

							  <br />

							  <table class="table">
							    <tbody>
							      <tr>
							        <td width="25%"><strong>Imuno Serologi</strong></td>
							        <td style="text-align: center !important"><strong>Hasil</strong></td>
							        <td style="text-align: center !important"><strong>Hasil Rujukan</strong></td>
							      </tr>
							      <tr>
							        <td width="25%">HBsAG</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->imuno->hbsag_status) }}</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->imuno->hbsag_reference) }}</td>
							      </tr>
							      <tr>
							        <td width="25%">ICT-Tuberculosis</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->imuno->ict_tbc) }}</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->imuno->ict_reference) }}</td>
							      </tr>
							    </tbody>
							  </table>

							  <br />
							  <p style="text-align: left !important; left: 12px !important; position: relative;"><strong>Urine Rutin</strong></p>

							  <table class="table" id="urine-table">
							    <tbody>
							      <tr>
							        <td width="25%"><strong>Makroskopis</strong></td>
							        <td style="text-align: center !important"><strong>Hasil</strong></td>
							        <td style="text-align: center !important"><strong>Hasil Rujukan</strong></td>
							      </tr>
							      <tr>
							        <td width="21%">Warna</td>
							        <td style="text-align: center !important;">{{ Helper::processUrine($report->urine->color_val) }}</td>
							        <td style="text-align: center !important;">{{ Helper::processUrine($report->urine->color_val_ref) }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Berat Jenis</td>
							        <td style="text-align: center !important;">{{ $report->urine->density_val }}</td>
							        <td style="text-align: center !important;">{{ $report->urine->density_val_ref }}</td>
							      </tr>
							       <tr>
							        <td width="21%">Ph</td>
							        <td style="text-align: center !important;">{{ $report->urine->ph_val }}</td>
							        <td style="text-align: center !important;">{{ $report->urine->ph_val_ref }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Lekosit Estrase</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->leukosit_estrase_val) }}</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->leukosit_estrase_val_ref) }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Nitrit</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->nitrit_val) }}</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->nitrit_val_ref) }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Protein</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->protein_val) }}</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->protein_val_ref) }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Glukosa</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->glucose_val) }}</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->glucose_val_ref) }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Keton</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->keton_val) }}</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->keton_val_ref) }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Urobilinogen</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->urobilinogen_val) }}</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->urobilinogen_val_ref) }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Bilirubin</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->bilirubin_val) }}</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->bilirubin_val_ref) }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Eritrosit</td>
							        <td style="text-align: center !important;">{{ $report->urine->eritrosit_val }}</td>
							        <td style="text-align: center !important;">{{ $report->urine->eritrosit_val_ref }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Lekosit</td>
							        <td style="text-align: center !important;">{{ $report->urine->leukosit_val }}</td>
							        <td style="text-align: center !important;">{{ $report->urine->leukosit_val_ref }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Epitel Cell</td>
							        <td style="text-align: center !important;">{{ $report->urine->epitel_val }}</td>
							        <td style="text-align: center !important;">{{ $report->urine->epitel_val_ref }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Bakteri</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->bacteria_val) }}</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->bacteria_val_ref) }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Silinder</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->silinder_val) }}</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->silinder_val_ref) }}</td>
							      </tr>
							      <tr>
							        <td width="21%">Kristal</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->kristal_val) }}</td>
							        <td style="text-align: center !important;">{{ Helper::processPolar($report->urine->kristal_val_ref) }}</td>
							      </tr>
							    </tbody>
							  </table>
						</div>
					</div>

				</div>
			</div>

			<div class="page-four">

				<div class="row">
					<div class="span10 offset1 doubleUnderline">
			     		<div class="span5"><p><strong>Tanggal : {{ date('d F Y', strtotime($report->check_date)) }}</strong></p></div>
  						<div class="span4"><p><strong>Bagian : {{ $employee->dpt->name }}</p></strong></div>
  						<div class="span5"><p><strong>Nama : {{ ucwords($employee->first_name . ' ' . $employee->middle_name . ' ' . $employee->last_name)  }}</p></strong></div>
  						<div class="span4"><p><strong>Telp : {{ $employee->phone ? $employee->phone : 'N/A' }}</p></strong></div>
  						<div class="span5"><p><strong>Usia/Kelamin : {{ $employee->age }} / {{ Helper::processGender($employee->gender) }}</p></strong></div>
  						<div class="span4"><p><strong>NIK : {{ $employee->nik }}</strong></p></div>
					</div>

					<div class="row">
				     	<div class="span10 offset1 text-center padding-top-10 padding-bottom-10">
				     		<h4>KESIMPULAN HASIL PEMERIKSAAN</h4>
						</div>
					</div>

					<div class="row">
						<div class="span10 offset1 text-center">
							<p style="text-align: left !important; left: 12px !important; position: relative;"><strong>I. Kesimpulan Hasil</strong></p>
						
							<table class="table">
							    <tbody>
							      <tr>
							        <td width="35%"><strong>&emsp; A. Pemeriksaan Fisik</strong></td>
							        <td width="5%"> :</td>
							        <td><strong>{{ $report->result->result_fisik }}</strong></td>
							      </tr>
							      <tr>
							        <td><strong>&emsp; B. Pemeriksaan Laboratorium</strong></td>
							        <td width="5%"></td>
							        <td></td>
							      </tr>
							      <tr>
							        <td>&emsp;&emsp; Hematologi Rutin</td>
							        <td width="5%">:</td>
							        <td>{{ $report->result->result_hema }}</td>
							      </tr>
							      <tr>
							        <td>&emsp;&emsp; Urin Rutin</td>
							        <td width="5%">:</td>
							        <td>{{ $report->result->result_urine }}</td>
							      </tr>
							      <tr>
							        <td><strong>&emsp;&emsp; Imuno Serologi</strong></td>
							        <td width="5%"></td>
							        <td></td>
							      </tr>
							      <tr>
							        <td>&emsp;&emsp;&emsp;&emsp; HbsAg (Hepatitis B)</td>
							        <td width="5%">:</td>
							        <td>{{ Helper::processPolar($report->result->result_hbsag) }}</td>
							      </tr>
							      <tr>
							        <td>&emsp;&emsp;&emsp;&emsp; ICT-Tuberculose</td>
							        <td width="5%">:</td>
							        <td>{{ Helper::processPolar($report->result->result_tbc) }}</td>
							      </tr>
							    </tbody>
							  </table>

							  <p style="text-align: left !important; left: 12px !important; position: relative;"><strong>II. Kesimpulan</strong></p>
							  <p style="text-align: left !important; left: 12px !important; position: relative;">&emsp; {{ $report->result->result_final }}</p>
						</div>
					</div>

				</div>
			</div>

		</div>

		<script src="{{ asset('assets/plugins/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/plugins/bootstrap2/js/bootstrap.min.js') }}" type="text/javascript"></script>

	</body>

</html>