@extends('layouts.backend')

@section('title')
	Formulir Medical Checkup
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Formulir Medical Checkup
@stop

@section('page_description')
	formulir pemeriksaan medis
@stop

@section('user_information')
	<div class="user-info">
		<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
			<div class="panel bg-white">
				<div class="panel-body padding-30-20">
					<div class="clearfix">
						<div class="pull-left">
							<div class="display-block color-blue-grey-400 font-weight-600">{{ strtoupper($employee->first_name . ' ' . $employee->middle_name . ' ' . $employee->last_name) }}</div>
							<div class="display-block color-blue-grey-400 font-weight-600">{{ $employee->nik }}</div>
						</div>
						<div class="pull-right">
							<i class="font-size-36 color-blue-grey-100 ion-person-add"></i>
						</div>
					</div>
				</div>
			</div><!-- /.panel -->
		</div><!-- /.col -->
	</div>
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<form class="form-horizontal" id="add-med" method="post" data-op="add-med">	

				@include('medcheck.general-add')

				@include('medcheck.hematology-add')

				@include('medcheck.imuno-add')
			
				@include('medcheck.urine-add')

				@include('medcheck.result-add')

			</form>

		</div>

	</div><!-- /.row -->
@stop

@section('scripts')

    <script src="{{ asset('assets/js/classes/med_check/MedCheck.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/classes/errors/med_check_errors.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/MedCheckManager.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {

			$("select").selectpicker();

		});
	</script>
	
@stop