<div class="panel wait-me-container" id="add-general-form">

	<div class="panel-title">
		<div class="panel-head">Pemeriksaan Fisik</div>
	</div>

	<div class="panel-body no-padding-left no-padding-right">
		
		<div class="form-group no-margin-left no-margin-right">
			<label for="name" class="col-lg-2 control-label"><span class="required">*</span> <span class="required-info">harus diisi</span></label>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="complain" class="col-lg-2 control-label">Keluhan saat ini <span class="required">*</span></label>
			<div class="col-lg-8">
				<textarea class="form-control bs-texteditor complain" rows="7" name="complain">{{ $report->complain }}</textarea>
				<span class="complain-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="medical_history" class="col-lg-2 control-label">Riwayat Penyakit <span class="required">*</span></label>
			<div class="col-lg-8">
				<textarea class="form-control bs-texteditor medical_history" rows="7" name="medical_history">{{ $report->medical_history }}</textarea>
				<span class="history-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="weight" class="col-lg-2 control-label">Berat Badan <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control weight" id="weight" name="weight" value="{{ $report->weight }}">
				<span class="weight-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>
			
		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="height" class="col-lg-2 control-label">Tinggi Badan <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control height" id="height" name="height" value="{{ $report->height }}">
				<span class="height-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="blood_pressure" class="col-lg-2 control-label">Tekanan Darah <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control blood_pressure" id="blood_pressure" name="blood_pressure" value="{{ $report->blood_pressure }}">
				<span class="blood-pressure-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="blood_pressure" class="col-lg-2 control-label">Gaya Hidup </label>
			<div class="col-lg-8">
				<div class="form-group">
					<div class="col-sm-12">
						<div class="checkbox checkbox-theme">
							<input type="checkbox" id="checkbox4" name="is_smoking" @if($report->is_smoking == '1'){{'checked'}}@endif>
							<label for="checkbox4">Merokok</label>
						</div>
						<div class="checkbox checkbox-theme">
							<input type="checkbox" id="checkbox5" name="is_consuming_alcohol" @if($report->is_consuming_alcohol == '1'){{'checked'}}@endif>
							<label for="checkbox5">Minum Alkohol</label>
						</div>
						<div class="checkbox checkbox-theme">
							<input type="checkbox" id="checkbox6" name="is_exercising" @if($report->is_exercising == '1'){{'checked'}}@endif>
							<label for="checkbox6">Olah Raga</label>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
			<label for="blood_pressure" class="col-lg-2 control-label"></label>
			<div class="col-lg-8">
				<div class="form-group">
					<div class="col-sm-12">
						<div class="checkbox checkbox-theme">
							<input type="checkbox" id="checkbox7" name="jvp" @if($report->jvp == '1'){{'checked'}}@endif>
							<label for="checkbox7">JVP</label>
						</div>
						<div class="checkbox checkbox-theme">
							<input type="checkbox" id="checkbox8" name="apex_beat" @if($report->apex_beat == '1'){{'checked'}}@endif>
							<label for="checkbox8">Apex Beat</label>
						</div>
						<div class="checkbox checkbox-theme">
							<input type="checkbox" id="checkbox9" name="cardiac_murmur" @if($report->cardiac_murmur == '1'){{'checked'}}@endif>
							<label for="checkbox9">Bising Jantung</label>
						</div>
						<div class="checkbox checkbox-theme">
							<input type="checkbox" id="checkbox10" name="murmur" @if($report->murmur == '1'){{'checked'}}@endif>
							<label for="checkbox10">Murmur</label>
						</div>
						<div class="checkbox checkbox-theme">
							<input type="checkbox" id="checkbox11" name="ronkhi" @if($report->ronkhi == '1'){{'checked'}}@endif>
							<label for="checkbox11">Ronkhi</label>
						</div>
						<div class="checkbox checkbox-theme">
							<input type="checkbox" id="checkbox12" name="wheezing" @if($report->wheezing == '1'){{'checked'}}@endif>
							<label for="checkbox12">Wheezing</label>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>