<div class="panel wait-me-container" id="add-hematology-form">

	<div class="panel-title">
		<div class="panel-head">Imuno Serologi Rutin</div>
	</div>

	<div class="panel-body no-padding-left no-padding-right">
		
		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="hbsag_status" class="col-lg-2 control-label">HbSag <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="hbsag_status" name="hbsag_status">
					<option value="1" @if($report->imuno->hbsag_status == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->imuno->hbsag_status == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="hbsag_reference" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="hbsag_reference" name="hbsag_reference">
					<option value="1" @if($report->imuno->hbsag_reference == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->imuno->hbsag_reference == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="ict_tbc" class="col-lg-2 control-label">ICT-Tuberculosis <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="ict_tbc" name="ict_tbc">
					<option value="1" @if($report->imuno->ict_tbc == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->imuno->ict_tbc == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="ict_reference" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="ict_reference" name="ict_reference">
					<option value="1" @if($report->imuno->ict_reference == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->imuno->ict_reference == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

	</div>

</div>	