@extends('layouts.backend')

@section('title')
	Daftar Laporan Karyawan - Index
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Daftar Laporan
@stop

@section('page_description')
	daftar laporan karyawan
@stop

@section('user_information')
	<div class="user-info">
		<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
			<div class="panel bg-white">
				<div class="panel-body padding-30-20">
					<div class="clearfix">
						<div class="pull-left">
							<div class="display-block color-blue-grey-400 font-weight-600">{{ strtoupper($employee->first_name . ' ' . $employee->middle_name . ' ' . $employee->last_name) }}</div>
							<div class="display-block color-blue-grey-400 font-weight-600">{{ $employee->nik }}</div>
						</div>
						<div class="pull-right">
							<i class="font-size-36 color-blue-grey-100 ion-person-add"></i>
						</div>
					</div>
				</div>
			</div><!-- /.panel -->
		</div><!-- /.col -->
	</div>
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

                <div class="panel-title bg-white no-border">
                	<div class="panel-tools float-left add-bottom-20">
						<a href="{{ URL::to('/') . '/medcheck/add/?id=' . $employee->id }}"><i class="fa fa-medkit"></i> Buat Laporan</a>
						<a href="" class="delete-all" data-employee-id="{{ $employee->id }}"><i class="fa fa-remove"></i> Hapus Laporan</a>
						<a href="{{ URL::to('/') . '/medcheck/printexcel/?id=' . $employee->id }}" target="_blank"><i class="fa fa-print"></i> Print Semua Laporan</a>
					</div>
				</div>

                <div class="panel-body no-padding-top bg-white">

					<table id="example1" class="table table-bordered table-striped">

						<thead>
							<tr>
								<th width="5"><input type="checkbox" id="selectall"></th>
								<th>Tanggal Periksa</th>
								<th>Actions</th>
							</tr>
						</thead>
						
						<tbody>
							@foreach($employee->medCheck as $check)
								<tr>
									<td width="5"><input type="checkbox" class="report" value="{{ $check->id }}"></td>
									<td>{{ $check->check_date }}</td>
									<td>
										<a title="Edit laporan" data-toggle="tooltip" href="{{ URL::to('/') . '/medcheck/edit?employee_id=' . $check->employee_id . '&report_id=' . $check->id }}" type="button" class="btn btn-success btn-circle edit" data-id="{{ $employee->id }}"><i class="glyphicon glyphicon-pencil"></i></a>
										<button title="Hapus laporan" data-toggle="tooltip" type="button" class="btn btn-warning btn-circle delete" data-employee-id="{{ $employee->id }}" data-report-id="{{ $check->id }}"><i class="glyphicon glyphicon-remove"></i></button>
										<a title="Print laporan" target="_blank" data-toggle="tooltip" href="{{ URL::to('/') . '/medcheck/printpdf?employee_id=' . $check->employee_id . '&report_id=' . $check->id }}" type="button" class="btn btn-success btn-circle edit" data-id="{{ $employee->id }}"><i class="glyphicon glyphicon-print"></i></a>
									</td>
								</tr>
							@endforeach
						</tbody>
						
						<tfoot>
							<tr>
								<th></th>
								<th>Tanggal Periksa</th>
								<th>Actions</th>
							</tr>
						</tfoot>

					</table>

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<!-- datatables -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
	</script>

	<script src="{{ asset('assets/js/classes/med_check/MedCheck.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/MedCheckManager.js') }}" type="text/javascript"></script>
@stop