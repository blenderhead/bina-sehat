<div class="panel wait-me-container" id="add-urine-form">

	<div class="panel-title">
		<div class="panel-head">Urine Rutin</div>
	</div>

	<div class="panel-body no-padding-left no-padding-right">

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="color_val" class="col-lg-2 control-label">Warna <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="color_val" name="color_val">
					@foreach(Config::get('urine_color') as $key => $value)
						<option value="{{ $key }}" @if($report->urine->color_val == $key){{'selected'}}@endif>{{ $value }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="color_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="color_val_ref" name="color_val_ref">
					@foreach(Config::get('urine_color') as $key => $value)
						<option value="{{ $key }}" @if($report->urine->color_val_ref == $key){{'selected'}}@endif>{{ $value }}</option>
					@endforeach
				</select>
			</div>
		</div>
		
		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="density_val" class="col-lg-2 control-label">Berat Jenis <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control density_val" id="density_val" name="density_val" value="{{ $report->urine->density_val }}">
				<span class="density-val-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="density_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control density_val_ref" id="density_val_ref" name="density_val_ref" value="{{ $report->urine->density_val_ref }}">
				<span class="density-val-ref-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-20">
			<label for="ph_val" class="col-lg-2 control-label">Ph <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control ph_val" id="ph_val" name="ph_val" value="{{ $report->urine->ph_val }}">
				<span class="ph-val-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="ph_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control ph_val_ref" id="ph_val_ref" name="ph_val_ref" value="{{ $report->urine->ph_val_ref }}">
				<span class="ph-val-ref-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="leukosit_estrase_val" class="col-lg-2 control-label">Lekosit Estrase <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="leukosit_estrase_val" name="leukosit_estrase_val">
					<option value="1" @if($report->urine->leukosit_estrase_val == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->leukosit_estrase_val == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="leukosit_estrase_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="leukosit_estrase_val_ref" name="leukosit_estrase_val_ref">
					<option value="1" @if($report->urine->leukosit_estrase_val_ref == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->leukosit_estrase_val_ref == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="nitrit_val" class="col-lg-2 control-label">Nitrit <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="nitrit_val" name="nitrit_val">
					<option value="1" @if($report->urine->nitrit_val == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->nitrit_val == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="nitrit_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="nitrit_val_ref" name="nitrit_val_ref">
					<option value="1" @if($report->urine->nitrit_val_ref == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->nitrit_val_ref == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="protein_val" class="col-lg-2 control-label">Protein <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="protein_val" name="protein_val">
					<option value="1" @if($report->urine->protein_val == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->protein_val == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="protein_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="protein_val_ref" name="protein_val_ref">
					<option value="1" @if($report->urine->protein_val_ref == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->protein_val_ref == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="glucose_val" class="col-lg-2 control-label">Glukosa <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="glucose_val" name="glucose_val">
					<option value="1" @if($report->urine->glucose_val == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->glucose_val == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="glucose_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="glucose_val_ref" name="glucose_val_ref">
					<option value="1" @if($report->urine->glucose_val_ref == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->glucose_val_ref == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="keton_val" class="col-lg-2 control-label">Keton <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="keton_val" name="keton_val">
					<option value="1" @if($report->urine->keton_val == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->keton_val == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="keton_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="keton_val_ref" name="keton_val_ref">
					<option value="1" @if($report->urine->keton_val_ref == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->keton_val_ref == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="urobilinogen_val" class="col-lg-2 control-label">Urobilinogen <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="urobilinogen_val" name="urobilinogen_val">
					<option value="1" @if($report->urine->urobilinogen_val == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->urobilinogen_val == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="urobilinogen_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="urobilinogen_val_ref" name="urobilinogen_val_ref">
					<option value="1" @if($report->urine->urobilinogen_val_ref == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->urobilinogen_val_ref == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-0">
			<label for="bilirubin_val" class="col-lg-2 control-label">Bilirubin <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="bilirubin_val" name="bilirubin_val">
					<option value="1" @if($report->urine->bilirubin_val == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->bilirubin_val == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="bilirubin_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="bilirubin_val_ref" name="bilirubin_val_ref">
					<option value="1" @if($report->urine->bilirubin_val_ref == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->bilirubin_val_ref == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="eritrosit_val" class="col-lg-2 control-label">Eritrosit <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control eritrosit_val" id="eritrosit_val" name="eritrosit_val" value="{{ $report->urine->eritrosit_val }}">
				<span class="eritrosit-val-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="eritrosit_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control eritrosit_val_ref" id="eritrosit_val_ref" name="eritrosit_val_ref" value="{{ $report->urine->eritrosit_val_ref }}">
				<span class="eritrosit-val-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="u_leukosit_val" class="col-lg-2 control-label">Lekosit <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control eritrosit_val" id="u_leukosit_val" name="u_leukosit_val" value="{{ $report->urine->leukosit_val }}">
				<span class="ulekosit-val-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="u_leukosit_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control u_leukosit_val_ref" id="u_leukosit_val_ref" name="u_leukosit_val_ref" value="{{ $report->urine->leukosit_val_ref }}">
				<span class="ulekosit-val-ref-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="epitel_val" class="col-lg-2 control-label">Epitel Cell <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control epitel_val" id="epitel_val" name="epitel_val" value="{{ $report->urine->epitel_val }}">
				<span class="epitel-val-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="epitel_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control epitel_val_ref" id="epitel_val_ref" name="epitel_val_ref" value="{{ $report->urine->epitel_val_ref }}">
				<span class="epitel-val-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="bacteria_val" class="col-lg-2 control-label">Bakteri <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="bacteria_val" name="bacteria_val">
					<option value="1" @if($report->urine->bacteria_val == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->bacteria_val == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="bacteria_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="bacteria_val_ref" name="bacteria_val_ref">
					<option value="1" @if($report->urine->bacteria_val_ref == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->bacteria_val_ref == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="silinder_val" class="col-lg-2 control-label">Silinder <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="silinder_val" name="silinder_val">
					<option value="1" @if($report->urine->silinder_val == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->silinder_val == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="silinder_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="silinder_val_ref" name="silinder_val_ref">
					<option value="1" @if($report->urine->silinder_val_ref == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->silinder_val_ref == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="kristal_val" class="col-lg-2 control-label">Kristal <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="kristal_val" name="kristal_val">
					<option value="1" @if($report->urine->kristal_val == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->kristal_val == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="kristal_val_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="kristal_val_ref" name="kristal_val_ref">
					<option value="1" @if($report->urine->kristal_val_ref == '1'){{'selected'}}@endif>Positif</option>
					<option value="0" @if($report->urine->kristal_val_ref == '0'){{'selected'}}@endif>Negatif</option>
				</select>
			</div>
		</div>

	</div>

</div>