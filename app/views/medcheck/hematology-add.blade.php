<div class="panel wait-me-container" id="add-hematology-form">

	<div class="panel-title">
		<div class="panel-head">Hematologi Rutin</div>
	</div>

	<div class="panel-body no-padding-left no-padding-right">
		
		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="hemoglobin_val" class="col-lg-2 control-label">Hb <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control hemoglobin_val" id="hemoglobin_val" name="hemoglobin_val">
				<span class="hemoglobin-val-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="hemoglobin_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control hemoglobin_ref" id="hemoglobin_ref" name="hemoglobin_ref">
				<span class="hemoglobin-ref-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>
			
		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="leukosit_val" class="col-lg-2 control-label">Lekosit <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control leukosit_val" id="leukosit_ref" name="leukosit_val">
				<span class="leukosit-val-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="leukosit_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control leukosit_ref" id="leukosit_ref" name="leukosit_ref">
				<span class="leukosit-ref-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="pcv_val" class="col-lg-2 control-label">Hematokrit <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control pcv_val" id="pcv_val" name="pcv_val">
				<span class="pcv-val-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="pcv_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control pcv_ref" id="pcv_ref" name="pcv_ref">
				<span class="pcv-ref-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="trombosit_val" class="col-lg-2 control-label">Trombosit <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control trombosit_val" id="trombosit_val" name="trombosit_val">
				<span class="trombosit-val-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="trombosit_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control trombosit_ref" id="trombosit_val" name="trombosit_ref">
				<span class="trombosit-ref-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="led_val" class="col-lg-2 control-label">LED <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control led_val" id="led_val" name="led_val">
				<span class="led-val-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-10 padding-bottom-10">
			<label for="led_ref" class="col-lg-2 control-label">Nilai Rujukan <span class="required">*</span></label>
			<div class="col-lg-8">
				<input type="text" class="form-control led_ref" id="led_ref" name="led_ref">
				<span class="led-ref-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

	</div>

</div>	