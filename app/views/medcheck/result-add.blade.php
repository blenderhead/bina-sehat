<div class="panel wait-me-container" id="add-general-form">

	<div class="panel-title">
		<div class="panel-head">Kesimpulan</div>
	</div>

	<div class="panel-body no-padding-left no-padding-right">
		
		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="result_fisik" class="col-lg-2 control-label">Cek Fisik <span class="required">*</span></label>
			<div class="col-lg-8">
				<textarea class="form-control bs-texteditor result_fisik" rows="7" name="result_fisik"></textarea>
				<span class="resultfis-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="result_hema" class="col-lg-2 control-label">Hematologi Rutin <span class="required">*</span></label>
			<div class="col-lg-8">
				<textarea class="form-control bs-texteditor result_hema" rows="7" name="result_hema"></textarea>
				<span class="resulthema-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="result_urine" class="col-lg-2 control-label">Urine Rutin <span class="required">*</span></label>
			<div class="col-lg-8">
				<textarea class="form-control bs-texteditor result_urine" rows="7" name="result_urine"></textarea>
				<span class="resulturine-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="result_hbsag" class="col-lg-2 control-label">HbSag <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="result_hbsag" name="result_hbsag">
					<option value="1">Positif</option>
					<option value="0">Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="result_tbc" class="col-lg-2 control-label">ICT-Tuberculosis <span class="required">*</span></label>
			<div class="col-lg-8">
				<select class="result_tbc" name="result_tbc">
					<option value="1">Positif</option>
					<option value="0">Negatif</option>
				</select>
			</div>
		</div>

		<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
			<label for="result_final" class="col-lg-2 control-label">Kesimpulan Akhir <span class="required">*</span></label>
			<div class="col-lg-8">
				<textarea class="form-control bs-texteditor result_final" rows="7" name="result_final"></textarea>
				<span class="resultfinal-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
			</div>
		</div>

		<div class="text-center margin-top-20 padding-top-20">
			<input type="hidden" name="employee_id" value="{{ $employee->id }}" class="employee_id" />
			<a href="{{ URL::to('/') . '/employee' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
			<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Save</button>
		</div>

	</div>

</div>