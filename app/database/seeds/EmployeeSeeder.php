<?php

    class EmployeeSeeder extends Seeder
    {
        public function run()
        {
            DB::table('employees')->truncate();

            for($i=1;$i<=20;$i++)
            {
                $employee = new Employee();
                $employee->nik = rand(100000,500000);
                $employee->first_name = 'Employee';
                $employee->last_name = "Test$i";
                $employee->age = rand(20,50);
                $employee->gender = str_shuffle('lp');
                $employee->department = rand(1,12);
                $employee->save();
            }
        }

    }
