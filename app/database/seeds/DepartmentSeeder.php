<?php

    class DepartmentSeeder extends Seeder
    {
        public function run()
        {
            DB::table('departments')->truncate();

            $departments = Config::get('departments');

            for($i=0;$i<count($departments);$i++)
            {
                $department = new Department();
                $department->name = $departments[$i];
                $department->save();
            }
        }

    }
