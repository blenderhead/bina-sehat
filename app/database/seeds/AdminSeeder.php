<?php

    class AdminSeeder extends Seeder
    {
        public function run()
        {
            DB::table('users')->truncate();

            $user = Sentry::getUserProvider()->create([
                'email' => 'admin@bina-sehat.dev',
                'password' => '12345678',
                'activated' => '1',
                'first_name' => 'Admin',
                'last_name' => 'BS',
            ]);
        }

    }
