<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEUrineChecksAddCheckId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('e_urine_checks', function(Blueprint $table)
		{
		    $table->integer('med_check_id')->after('employee_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('e_urine_checks', function(Blueprint $table)
		{
		    $table->dropColumn('med_check_id');
		});
	}

}
