<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEImuno extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('e_imuno', function(Blueprint $table)
		{
		    $table->increments('id');
		    $table->integer('employee_id');
		    $table->integer('med_check_id');
		    $table->string('hbsag_status');
		    $table->string('hbsag_reference');
		    $table->string('ict_tbc');
		    $table->string('ict_reference');
		    $table->date('check_date');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('e_imuno');
	}

}
