<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEChecksAddMore extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('e_checks', function(Blueprint $table)
		{
		    $table->text('complain')->after('employee_id');
		    $table->text('medical_history')->after('complain');
		    $table->integer('jvp')->after('is_exercising');
		    $table->integer('apex_beat')->after('jvp');
		    $table->integer('cardiac_murmur')->after('apex_beat');
		    $table->integer('murmur')->after('cardiac_murmur');
		    $table->integer('ronkhi')->after('murmur');
		    $table->integer('wheezing')->after('ronkhi');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('e_checks', function(Blueprint $table)
		{
		    $table->dropColumn('complain');
		    $table->dropColumn('medical_history');
		    $table->dropColumn('jvp');
		    $table->dropColumn('apex_beat');
		    $table->dropColumn('cardiac_murmur');
		    $table->dropColumn('murmur');
		    $table->dropColumn('ronkhi');
		    $table->dropColumn('wheezing');
		});
	}

}
