<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEResults extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('e_results', function(Blueprint $table)
		{
		    $table->increments('id');
		    $table->integer('employee_id');
		    $table->integer('med_check_id');
		    $table->text('result_fisik');
		    $table->text('result_hema');
		    $table->text('result_urine');
		    $table->string('result_hbsag');
		    $table->string('result_tbc');
		    $table->text('result_final');
		    $table->date('check_date');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('e_results');
	}

}
