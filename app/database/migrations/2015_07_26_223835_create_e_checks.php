<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEChecks extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('e_checks', function(Blueprint $table)
		{
		    $table->increments('id');
		    $table->integer('employee_id');
		    $table->integer('weight');
		    $table->integer('height');
		    $table->string('blood_pressure');
		    $table->integer('is_smoking');
		    $table->integer('is_consuming_alcohol');
		    $table->integer('is_exercising');
		    $table->date('check_date');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('e_checks');
	}

}
