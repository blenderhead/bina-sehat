<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEUrineChecks extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('e_urine_checks', function(Blueprint $table)
		{
		    $table->increments('id');
		    $table->integer('employee_id');
		    $table->string('color_val');
		    $table->float('ph_val');
		    $table->float('density_val');
		    $table->string('other_val');
		    $table->string('epitel_val');
		    $table->string('leukosit_val');
		    $table->string('eritrosit_val');
		    $table->string('bacteria_status');
		    $table->date('check_date');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('e_urine_checks');
	}

}
