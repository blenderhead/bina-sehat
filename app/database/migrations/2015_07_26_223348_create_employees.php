<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployees extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employees', function(Blueprint $table)
		{
		    $table->increments('id');
		    $table->string('nik');
		    $table->string('first_name');
		    $table->string('middle_name')->nullable();
		    $table->string('last_name')->nullable();
		    $table->integer('age');
		    $table->char('gender', 1);
		    $table->integer('department');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employees');
	}

}
