<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEHematologiesAddRefs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('e_hematologies', function(Blueprint $table)
		{
		    $table->string('hemoglobin_ref')->after('hemoglobin_val');
		    $table->string('leukosit_ref')->after('leukosit_val');
		    $table->string('pcv_ref')->after('pcv_val');
		    $table->string('trombosit_ref')->after('trombosit_val');
		    $table->string('led_ref')->after('led_val');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('e_hematologies', function(Blueprint $table)
		{
		    $table->dropColumn('hemoglobin_ref');
		    $table->dropColumn('leukosit_ref');
		    $table->dropColumn('pcv_ref');
		    $table->dropColumn('trombosit_ref');
		    $table->dropColumn('led_ref');
		});
	}

}
