<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEUrineAddRefs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('e_urine_checks', function(Blueprint $table)
		{
		    $table->string('color_val_ref')->after('color_val');
		    $table->string('density_val_ref')->after('density_val');
		    $table->string('ph_val_ref')->after('ph_val');
		    $table->string('leukosit_estrase_val')->after('ph_val_ref');
		    $table->string('leukosit_estrase_val_ref')->after('leukosit_estrase_val');
		    $table->string('nitrit_val')->after('leukosit_estrase_val_ref');
		    $table->string('nitrit_val_ref')->after('nitrit_val');
		    $table->string('protein_val')->after('nitrit_val_ref');
		    $table->string('protein_val_ref')->after('protein_val');
		    $table->string('glucose_val')->after('protein_val_ref');
		    $table->string('glucose_val_ref')->after('glucose_val');
		    $table->string('keton_val')->after('glucose_val_ref');
		    $table->string('keton_val_ref')->after('keton_val');
		    $table->string('urobilinogen_val')->after('keton_val_ref');
		    $table->string('urobilinogen_val_ref')->after('urobilinogen_val');
		    $table->string('bilirubin_val')->after('urobilinogen_val_ref');
		    $table->string('bilirubin_val_ref')->after('bilirubin_val');
		    $table->string('eritrosit_val_ref')->after('eritrosit_val');
		    $table->string('leukosit_val_ref')->after('leukosit_val');
		    $table->string('epitel_val_ref')->after('epitel_val');
		    $table->string('bacteria_val')->after('bacteria_status');
		    $table->string('bacteria_val_ref')->after('bacteria_val');
		    $table->string('silinder_val')->after('bacteria_val_ref');
		    $table->string('silinder_val_ref')->after('silinder_val');
		    $table->string('kristal_val')->after('silinder_val_ref');
		    $table->string('kristal_val_ref')->after('kristal_val');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('e_urine_checks', function(Blueprint $table)
		{
		    $table->dropColumn('color_val_ref');
		    $table->dropColumn('density_val_ref');
		    $table->dropColumn('ph_val_ref');
		    $table->dropColumn('leukosit_estrase_val');
		    $table->dropColumn('leukosit_estrase_val_ref');
		    $table->dropColumn('nitrit_val');
		    $table->dropColumn('nitrit_val_ref');
		    $table->dropColumn('protein_val');
		    $table->dropColumn('protein_val_ref');
		    $table->dropColumn('glucose_val');
		    $table->dropColumn('glucose_val_ref');
		    $table->dropColumn('keton_val');
		    $table->dropColumn('keton_val_ref');
		    $table->dropColumn('urobilinogen_val');
		    $table->dropColumn('urobilinogen_val_ref');
		    $table->dropColumn('bilirubin_val');
		    $table->dropColumn('bilirubin_val_ref');
		    $table->dropColumn('eritrosit_val_ref');
		    $table->dropColumn('leukosit_val_ref');
		    $table->dropColumn('epitel_val_ref');
		    $table->dropColumn('bacteria_val');
		    $table->dropColumn('bacteria_val_ref');
		    $table->dropColumn('silinder_val');
		    $table->dropColumn('silinder_val_ref');
		    $table->dropColumn('kristal_val');
		    $table->dropColumn('kristal_val_ref');
		});
	}

}
