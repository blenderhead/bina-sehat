<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEHematologies extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('e_hematologies', function(Blueprint $table)
		{
		    $table->increments('id');
		    $table->integer('employee_id');
		    $table->float('hemoglobin_val');
		    $table->float('leukosit_val');
		    $table->integer('pcv_val');
		    $table->integer('trombosit_val');
		    $table->integer('led_val');
		    $table->string('hbsag_status');
		    $table->date('check_date');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('e_hematologies');
	}

}
