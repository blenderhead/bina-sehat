$(document).ready(function() {

    $("#selectall").removeProp("checked");
    $(".report").removeProp("checked");
    
    var report_id = [];

    $('body').on('click', '.delete', function() {
        var employee_id = $(this).data('employee-id');
        var report_id = [$(this).data('report-id')];
        var url = baseUrl + '/medcheck/deletebydate';
        var redirect_url = baseUrl + '/report/master';
        deleteData(report_id, url, redirect_url);
    });

    $('body').on('click', '.delete-all', function(e) {

        e.preventDefault();

        if(report_id.length > 0)
        {
            var employee_id = $(this).data('employee-id');
            var rid = report_id;
            var url = baseUrl + '/medcheck/deletebydate';
            var redirect_url = baseUrl + '/report/master';
            deleteData(rid, url, redirect_url);    
        }
        
    });

    $("#selectall").click(function () {

        $('.report').prop('checked', this.checked);

        if(this.checked)
        {
            report_id.splice(0,report_id.length);

            $(".report").each(function(index, value) {

                report_id.push($(this).val());

            });

            console.log(report_id);
        }
        else
        {
            report_id.splice(0,report_id.length);
            console.log(report_id);
        }

    });
 
    $(".report").click(function(){
 
        if($(".report").length == $(".report:checked").length) 
        {
            $("#selectall").prop("checked", "checked");
        } 
        else 
        {
            $("#selectall").removeProp("checked");
        }

        if(this.checked)
        {
            report_id.push($(this).val());
            console.log(report_id);
        }
        else
        {
            report_id.splice( $.inArray($(this).val(), report_id), 1 );
            console.log(report_id);
        }
 
    });

});