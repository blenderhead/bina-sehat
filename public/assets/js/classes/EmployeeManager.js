var author = new Employee();

$(document).ready(function() {

    $("#selectall").removeProp("checked");
    $(".employee").removeProp("checked");

    var employee_id = [];

	$('#add-employee, #edit-employee').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
        	case 'add':
        		var url = baseUrl + '/employee/add';
        		break;

        	case 'edit':
        		var url = baseUrl + '/employee/edit';
        		break;
        }
        
		$.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showEmployeeError(data);
                    showError(data);
                }
                else
                {
                    switch(op)
                    {
                        case 'add':
                            var redirect_url = baseUrl + '/medcheck/add?id=' + data.data.employee_id;
                            break;

                        case 'edit':
                            var redirect_url = baseUrl + '/employee/edit?id=' + $('.employee_id').val();
                            break;
                    }

                    showSuccess('Karyawan berhasil disimpan', redirect_url);
                }

            }
        });
    });
    
    $('body').on('click', '.delete', function() {
        var id = [$(this).data('id')];
        var url = baseUrl + '/employee/delete';
        var redirect_url = baseUrl + '/employee';
        deleteData(id, url, redirect_url);
    });

    $('body').on('click', '.delete-all', function(e) {
        e.preventDefault();

        if(employee_id.length > 0)
        {
            var id = employee_id;
            var url = baseUrl + '/employee/delete';
            var redirect_url = baseUrl + '/employee';
            deleteData(id, url, redirect_url);    
        }
        
    });

    $('body').on('click', '#selectall', function(e) {

        $('.employee').prop('checked', this.checked);

        if(this.checked)
        {
            employee_id.splice(0,employee_id.length);

            $(".employee").each(function(index, value) {

                employee_id.push($(this).val());

            });

            console.log(employee_id);
        }
        else
        {
            employee_id.splice(0,employee_id.length);
            console.log(employee_id);
        }

    });
 
    $('body').on('click', '.employee', function(e) {
 
        if($(".employee").length == $(".employee:checked").length) 
        {
            $("#selectall").prop("checked", "checked");
        } 
        else 
        {
            $("#selectall").removeProp("checked");
        }

        if(this.checked)
        {
            employee_id.push($(this).val());
            console.log(employee_id);
        }
        else
        {
            employee_id.splice( $.inArray($(this).val(), employee_id), 1 );
            console.log(employee_id);
        }
 
    });

    $('#import-employee').submit(function(e) {

        e.preventDefault();
        
        var form_data = new FormData(this);

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: baseUrl + '/employee/import',
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showEmployeeError(data);
                    showError(data);
                }
                else
                {
                    showSuccess('Data berhasil diimport', baseUrl + '/employee/');
                }

            }
        });
    });
});