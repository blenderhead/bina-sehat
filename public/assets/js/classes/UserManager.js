var user = new User();

$(document).ready(function() {
 	
	$('#login-form').submit(function(e) {

        e.preventDefault();

        var form_data = new FormData(this);
        
		$.ajax({
            type: 'POST',
            dataType: 'json',
            url: baseUrl + '/login',
            data: form_data,
            processData: false,
            contentType: false,
            success: function(data) {

                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    window.location.replace(baseUrl + '/dashboard');
                }

            }
        });
    });

});