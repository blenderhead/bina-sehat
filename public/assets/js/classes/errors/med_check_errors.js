function showGeneralError(data)
{
    if(data.data.errors.complain != undefined)
    {
        $('.complain').addClass("border-red-300 bg-red-50 color-red-800");
        $('.complain-error').removeClass('hide');
    }
    else
    {
        $('.complain').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.complain-error').addClass('hide');
    }

    if(data.data.errors.medical_history != undefined)
    {
        $('.medical_history').addClass("border-red-300 bg-red-50 color-red-800");
        $('.history-error').removeClass('hide');
    }
    else
    {
        $('.medical_history').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.history-error').addClass('hide');
    }

    if(data.data.errors.weight != undefined)
    {
        $('.weight').addClass("border-red-300 bg-red-50 color-red-800");
        $('.weight-error').removeClass('hide');
    }
    else
    {
        $('.weight').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.weight-error').addClass('hide');
    }

    if(data.data.errors.height != undefined)
    {
        $('.height').addClass("border-red-300 bg-red-50 color-red-800");
        $('.height-error').removeClass('hide');
    }
    else
    {
        $('.height').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.height-error').addClass('hide');
    }

    if(data.data.errors.blood_pressure != undefined)
    {
        $('.blood_pressure').addClass("border-red-300 bg-red-50 color-red-800");
        $('.blood-pressure-error').removeClass('hide');
    }
    else
    {
        $('.blood_pressure').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.blood-pressure-error').addClass('hide');
    }
}

function showHemaError(data)
{
    if(data.data.errors.hemoglobin_val != undefined)
    {
        $('.hemoglobin_val').addClass("border-red-300 bg-red-50 color-red-800");
        $('.hemoglobin-val-error').removeClass('hide');
    }
    else
    {
        $('.hemoglobin_val').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.hemoglobin-val-error').addClass('hide');
    }

    if(data.data.errors.hemoglobin_ref != undefined)
    {
        $('.hemoglobin_ref').addClass("border-red-300 bg-red-50 color-red-800");
        $('.hemoglobin-ref-error').removeClass('hide');
    }
    else
    {
        $('.hemoglobin_ref').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.hemoglobin-ref-error').addClass('hide');
    }

    if(data.data.errors.leukosit_val != undefined)
    {
        $('.leukosit_val').addClass("border-red-300 bg-red-50 color-red-800");
        $('.leukosit-val-error').removeClass('hide');
    }
    else
    {
        $('.leukosit_val').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.leukosit-val-error').addClass('hide');
    }

    if(data.data.errors.leukosit_ref != undefined)
    {
        $('.leukosit_ref').addClass("border-red-300 bg-red-50 color-red-800");
        $('.leukosit-ref-error').removeClass('hide');
    }
    else
    {
        $('.leukosit_ref').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.leukosit-ref-error').addClass('hide');
    }

    if(data.data.errors.pcv_val != undefined)
    {
        $('.pcv_val').addClass("border-red-300 bg-red-50 color-red-800");
        $('.pcv-val-error').removeClass('hide');
    }
    else
    {
        $('.pcv_val').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.pcv-val-error').addClass('hide');
    }

    if(data.data.errors.pcv_ref != undefined)
    {
        $('.pcv_ref').addClass("border-red-300 bg-red-50 color-red-800");
        $('.pcv-val-error').removeClass('hide');
    }
    else
    {
        $('.pcv_ref').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.pcv-ref-error').addClass('hide');
    }

    if(data.data.errors.trombosit_val != undefined)
    {
        $('.trombosit_val').addClass("border-red-300 bg-red-50 color-red-800");
        $('.trombosit-val-error').removeClass('hide');
    }
    else
    {
        $('.trombosit_val').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.trombosit-val-error').addClass('hide');
    }

    if(data.data.errors.trombosit_ref != undefined)
    {
        $('.trombosit_ref').addClass("border-red-300 bg-red-50 color-red-800");
        $('.trombosit-ref-error').removeClass('hide');
    }
    else
    {
        $('.trombosit_ref').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.trombosit-ref-error').addClass('hide');
    }

    if(data.data.errors.led_val != undefined)
    {
        $('.led_val').addClass("border-red-300 bg-red-50 color-red-800");
        $('.led-val-error').removeClass('hide');
    }
    else
    {
        $('.led_val').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.led-val-error').addClass('hide');
    }

    if(data.data.errors.led_ref != undefined)
    {
        $('.led_ref').addClass("border-red-300 bg-red-50 color-red-800");
        $('.led-ref-error').removeClass('hide');
    }
    else
    {
        $('.led_ref').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.led-ref-error').addClass('hide');
    }
}

function showUrineError(data)
{
    if(data.data.errors.ph_val != undefined)
    {
        $('.ph_val').addClass("border-red-300 bg-red-50 color-red-800");
        $('.ph-val-error').removeClass('hide');
    }
    else
    {
        $('.ph_val').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.ph-val-error').addClass('hide');
    }

    if(data.data.errors.ph_val_ref != undefined)
    {
        $('.ph_val_ref').addClass("border-red-300 bg-red-50 color-red-800");
        $('.ph-val-ref-error').removeClass('hide');
    }
    else
    {
        $('.ph_val_ref').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.ph-val-ref-error').addClass('hide');
    }

    if(data.data.errors.density_val != undefined)
    {
        $('.density_val').addClass("border-red-300 bg-red-50 color-red-800");
        $('.density-val-error').removeClass('hide');
    }
    else
    {
        $('.density_val').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.density-val-error').addClass('hide');
    }

    if(data.data.errors.density_val_ref != undefined)
    {
        $('.density_val_ref').addClass("border-red-300 bg-red-50 color-red-800");
        $('.density-val-ref-error').removeClass('hide');
    }
    else
    {
        $('.density_val_ref').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.density-val-error').addClass('hide');
    }

    if(data.data.errors.epitel_val != undefined)
    {
        $('.epitel_val').addClass("border-red-300 bg-red-50 color-red-800");
        $('.epitel-val-ref-error').removeClass('hide');
    }
    else
    {
        $('.epitel_val').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.epitel-val-error').addClass('hide');
    }

    if(data.data.errors.epitel_val_ref != undefined)
    {
        $('.epitel_val_ref').addClass("border-red-300 bg-red-50 color-red-800");
        $('.epitel-val-ref-error').removeClass('hide');
    }
    else
    {
        $('.epitel_val_ref').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.epitel-val-ref-error').addClass('hide');
    }

    if(data.data.errors.u_leukosit_val != undefined)
    {
        $('.u_leukosit_val').addClass("border-red-300 bg-red-50 color-red-800");
        $('.uleukosit-val-error').removeClass('hide');
    }
    else
    {
        $('.u_leukosit_val').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.uleukosit-val-error').addClass('hide');
    }

    if(data.data.errors.u_leukosit_val_ref != undefined)
    {
        $('.u_leukosit_val_ref').addClass("border-red-300 bg-red-50 color-red-800");
        $('.uleukosit-val-ref-error').removeClass('hide');
    }
    else
    {
        $('.u_leukosit_val_ref').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.uleukosit-val-ref-error').addClass('hide');
    }

    if(data.data.errors.eritrosit_val != undefined)
    {
        $('.eritrosit_val').addClass("border-red-300 bg-red-50 color-red-800");
        $('.eritrosit-val-error').removeClass('hide');
    }
    else
    {
        $('.eritrosit_val').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.eritrosit-val-error').addClass('hide');
    }

    if(data.data.errors.eritrosit_val_ref != undefined)
    {
        $('.eritrosit_val_ref').addClass("border-red-300 bg-red-50 color-red-800");
        $('.eritrosit-val-ref-error').removeClass('hide');
    }
    else
    {
        $('.eritrosit_val_ref').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.eritrosit-val-ref-error').addClass('hide');
    }
}

function showResultError(data)
{
    if(data.data.errors.result_fisik != undefined)
    {
        $('.result_fisik').addClass("border-red-300 bg-red-50 color-red-800");
        $('.resultfis-error').removeClass('hide');
    }
    else
    {
        $('.result_fisik').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.resulthema-error').addClass('hide');
    }

    if(data.data.errors.result_hema != undefined)
    {
        $('.result_hema').addClass("border-red-300 bg-red-50 color-red-800");
        $('.resulthema-error').removeClass('hide');
    }
    else
    {
        $('.result_hema').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.resulthema-error').addClass('hide');
    }

    if(data.data.errors.result_urine != undefined)
    {
        $('.result_urine').addClass("border-red-300 bg-red-50 color-red-800");
        $('.resulturine-error').removeClass('hide');
    }
    else
    {
        $('.result_urine').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.resulturine-error').addClass('hide');
    }

    if(data.data.errors.result_final != undefined)
    {
        $('.result_final').addClass("border-red-300 bg-red-50 color-red-800");
        $('.resultfinal-error').removeClass('hide');
    }
    else
    {
        $('.result_final').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.resultfinal-error').addClass('hide');
    }
}