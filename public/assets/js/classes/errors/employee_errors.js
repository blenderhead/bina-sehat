function showEmployeeError(data)
{
    if(data.data.errors.nik != undefined)
    {
        $('.employee_nik').addClass("border-red-300 bg-red-50 color-red-800");
        $('.nik-error').removeClass('hide');
    }
    else
    {
        $('.employee_nik').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.nik-error').addClass('hide');
    }

    if(data.data.errors.first_name != undefined)
    {
        $('.employee_first_name').addClass("border-red-300 bg-red-50 color-red-800");
        $('.first-name-error').removeClass('hide');
    }
    else
    {
        $('.employee_first_name').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.first-name-error').addClass('hide');
    }

    if(data.data.errors.age != undefined)
    {
        $('.employee_age').addClass("border-red-300 bg-red-50 color-red-800");
        $('.age-error').removeClass('hide');
    }
    else
    {
        $('.employee_age').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.age-error').addClass('hide');
    }
}