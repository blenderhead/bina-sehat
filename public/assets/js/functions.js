function showError(data)
{
	var msg = '';

	if (data.error == '9107') 
	{
	  msg = '<div><strong>' + data.message + '</strong></div>';

	  for (var x in data.data.errors) {
	    msg += '<div><strong><em>- ' + data.data.errors[x][0] + '</em></strong></div>';
	  }

	  bootbox.alert(msg, function() {});
	}
	else if (data.error == '1000') 
	{
	  msg = '<div><p><strong>' + data.message + '</strong></p></div>';
	  msg += '<div><strong><em>' + data.data.errors + '</em></strong></div>';

	  bootbox.alert(msg, function() {});
	}
	else 
	{
	  bootbox.alert(data.message, function() {});
	}
}

function showSuccess(msg,url)
{
	bootbox.alert(msg, function() {
		window.location.replace(url);
	});
}

function showOpTooltip()
{
    $(".edit").tooltip({
        title : 'Edit'
    });

    $(".delete").tooltip({
        title : 'Delete'
    });
}

function deleteData(id, url, redirect_url)
{
    bootbox.confirm("Are you sure?", function(result) {
        
        if(result)
        {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: { 
                    'id' : id,
                },
                url: url,
                beforeSend: function() {
                    $('.panel').waitMe({
                        effect : 'stretch',
                        text : 'Deleting...',
                        bg : 'rgba(255,255,255,0.7)',
                        color : '#000',
                        sizeW : '',
                        sizeH : ''
                    });
                },
                success: function(data) {
                    if(data.error)
                    {
                        showError(data);
                    }
                    else
                    {
                        showSuccess('Data is successfully deleted', redirect_url);
                    }
                },
                complete: function() {
                    $('.panel').waitMe('hide');
                } 
            });
        }
    }); 
}

function cancelOp(redirect_url)
{
    window.location.replace(redirect_url);
}